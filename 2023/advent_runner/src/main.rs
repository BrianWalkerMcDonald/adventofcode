use common::AdventDay;
use day01::Day1;

fn main() {
    println!("Hello advent of code!");

    // TODO: how to iterate of structs as namespaces, boxes didn't work

    println!("-- Day 1 --");
    println!("part1 answer: {:?}", Day1::part1());
    println!("part2 answer: {:?}", Day1::part2());
}
