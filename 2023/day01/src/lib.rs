use std::io;
use std::path::Path;
use std::fs::File;

use common::AdventDay;
pub struct Day1;

impl AdventDay<u32> for Day1 {
    fn part1() -> u32 {
        let iterator = CalibrationIterator::from_file("day01/data/input_part1.data").unwrap();

        iterator.sum()
    }

    fn part2() -> u32 {
        0
    }
}

struct CalibrationIterator<R> 
where R: io::BufRead{
    lines_iterator: R,
}

impl CalibrationIterator<io::BufReader<File>> {

    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { lines_iterator: io::BufReader::new(file) })
    }

}

impl<R: io::BufRead> Iterator for CalibrationIterator<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {

        // Read the next line into a buffer.
        let mut buf = String::new();
        let mut bytes = self.lines_iterator.read_line(&mut buf).unwrap();
        
        if bytes == 0 || buf == "\n"{
            return None;
        }

        let mut first_digit: Option<u32> = None;
        let mut last_digit: Option<u32> = None;

        for character in buf.chars() {
            match character.to_digit(10) {
                Some(digit) => {
                    if first_digit.is_none() {
                        first_digit = Some(digit);
                    }
                    last_digit = Some(digit);
                },
                None => {
                    continue;
                }
            }
        }

        match (first_digit, last_digit) {
            (Some(first), Some(last)) => Some(first * 10 + last),
            _ => panic!("Invalid input")
        }
    }
}

impl Day1 {


    #[allow(dead_code)]
    fn example_part1() -> u32 {
        let iterator = CalibrationIterator::from_file("data/example_one.data").unwrap();

        iterator.sum()
    }

    #[allow(dead_code)]
    fn example_part2() -> u32 {
        let iterator = CalibrationIterator::from_file("data/example_two.data").unwrap();

        iterator.sum()
    }

}

#[cfg(test)]
mod tests {
    use super::Day1;

    #[test]
    fn example_part1() {
        assert_eq!(Day1::example_part1(), 142);
    }

    #[test]
    fn example_part2() {
        assert_eq!(Day1::example_part2(), 281);
    }
}