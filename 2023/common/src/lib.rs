use std::fmt::Debug;

pub trait AdventDay<T> 
where T: Debug {
    fn part1() -> T;
    fn part2() -> T;
}
