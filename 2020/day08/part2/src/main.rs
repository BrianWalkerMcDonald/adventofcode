use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::{HashSet};

enum Commands {
	NoOp(i32),
	Acc(i32),
	Jmp(i32)
}

enum ExitStates {
	Recursion,
	EndOfProgram(i32),
	OutOfBounds
}

struct Emulator {
	memory: Vec<Commands>,
	accumulator: i32,
	command_pointer: i32,
	command_tracker: HashSet<i32>,
}

impl Emulator { 

	fn from_file(input_filename: &str) -> Emulator{
		let input_file = File::open(input_filename).expect("Unable to open file");
		let buffer = BufReader::new(input_file);

		let memory: Vec<Commands> = buffer
			.lines()
			.map(| line | line.unwrap() )
			.map(| line | {

				let line = line.trim();
				let split_line: Vec<&str> = line.split(' ').collect();


				match &split_line[..] {
					["nop", value] => Commands::NoOp(value.parse::<i32>().unwrap()),
					["acc", value] => Commands::Acc(value.parse::<i32>().unwrap()),
					["jmp", value] => Commands::Jmp(value.parse::<i32>().unwrap()),
					_ => { panic!("Unknown command"); },
				}
			})
			.collect();


		Emulator { memory, accumulator: 0, command_pointer: 0,  command_tracker: HashSet::new()}
	}

	fn execute_command(&mut self) -> Result<(), ExitStates> {	

		let option_command = self.memory.get(self.command_pointer as usize);
		let command = match option_command {
			None => { return Err(ExitStates::OutOfBounds); },
			Some(x) => x,
		};

		match command {
			Commands::Acc(x) => { self.accumulator += x; },
			Commands::NoOp(_) => { },
			Commands::Jmp(x) => { self.command_pointer +=  x - 1 },
		}

		self.command_pointer += 1;

		Ok(())
	}


	fn execute_program(&mut self) -> ExitStates {
		loop {
			self.command_tracker.insert(self.command_pointer);

			if let Err(err) = self.execute_command() {
				return err;
			};

			if self.command_tracker.contains(&self.command_pointer) {
				return ExitStates::Recursion;
			}

			// Is a jump to the end valid? Ignoring that
			if self.command_pointer == ( self.memory.len() as i32 ){
				return ExitStates::EndOfProgram(self.accumulator)
			}
		}
	}

	fn replacement_execute_command(&mut self) -> Result<bool, ExitStates> {
		let option_command = self.memory.get(self.command_pointer as usize);
		let command = match option_command {
			None => { return Err(ExitStates::OutOfBounds); },
			Some(x) => x,
		};

		let replacement = match command {
			Commands::Acc(x) => { self.accumulator += x; false },
			Commands::NoOp(x) => {self.command_pointer +=  x - 1; true },
			Commands::Jmp(_) => { true },
		};

		self.command_pointer += 1;

		Ok(replacement)
	}

	fn replacemet_execute_program(&mut self) -> i32 {

		loop {

			let save_ptr = self.command_pointer;
			let accumulator = self.accumulator;

			self.command_tracker.insert(self.command_pointer);
			let result = self.replacement_execute_command();
			
			if self.command_tracker.contains(&self.command_pointer) {
				self.command_pointer = save_ptr;
				self.accumulator = accumulator;

				let result = self.execute_command();
				if let Err(ExitStates::OutOfBounds) = result {
					panic!("Out of Bounds");
				} 
			}

			if let Ok(true) = result {

				let program_exit = self.execute_program();

				if let ExitStates::EndOfProgram(value) = program_exit {
					return value;
				}

				self.command_pointer = save_ptr;
				self.accumulator = accumulator;

				let result = self.execute_command();
				if let Err(ExitStates::OutOfBounds) = result {
					panic!("Out of Bounds");
				} 
			}
		}

	}
}


fn main() {
	let mut emulator = Emulator::from_file("input.txt");
	
	let answer = emulator.replacemet_execute_program();

	println!("The accumulator is a value of {}", answer);
}

