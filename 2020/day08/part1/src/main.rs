use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::{HashSet};

enum Commands {
	NoOp,
	Acc(i32),
	Jmp(i32)
}



struct Emulator {
	memory: Vec<Commands>,
	accumulator: i32,
	command_pointer: i32,
}

impl Emulator { 

	fn from_file(input_filename: &str) -> Emulator{
		let input_file = File::open(input_filename).expect("Unable to open file");
		let buffer = BufReader::new(input_file);

		let memory: Vec<Commands> = buffer
			.lines()
			.map(| line | line.unwrap() )
			.map(| line | {

				let line = line.trim();
				let split_line: Vec<&str> = line.split(' ').collect();

				match &split_line[..] {
					["nop", _] => Commands::NoOp,
					["acc", value] => Commands::Acc(value.parse::<i32>().unwrap()),
					["jmp", value] => Commands::Jmp(value.parse::<i32>().unwrap()),
					_ => { panic!("Unknown command"); },
				}
			})
			.collect();


		Emulator { memory, accumulator: 0, command_pointer: 0 }
	}

	fn execute_command(&mut self) -> Result<(), ()> {	

		let option_command = self.memory.get(self.command_pointer as usize);
		let command = match option_command {
			None => { return Err(()); },
			Some(x) => x,
		};

		match command {
			Commands::Acc(x) => { self.accumulator += x; },
			Commands::NoOp => { },
			Commands::Jmp(x) => { self.command_pointer +=  x - 1 },
		}

		self.command_pointer += 1;

		Ok(())
	}
}


fn main() {
	let mut emulator = Emulator::from_file("input.txt");
	
	let mut command_tracker = HashSet::new();
	
	while !command_tracker.contains(&emulator.command_pointer) {
		command_tracker.insert(emulator.command_pointer);
		let result = emulator.execute_command();
		match result {
			Err(()) => { panic!("Error");},
			Ok(_) => { },
		}
	}

	println!("The accumulator is a value of {}", emulator.accumulator);

}

