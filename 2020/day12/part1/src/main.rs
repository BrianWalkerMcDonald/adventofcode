use std::fs::File;
use std::io::{BufReader, BufRead};
use std::iter::Cycle;
use std::slice::Iter;


#[derive(PartialEq, Clone, Copy)]
enum Direction {
	North,
	East,
	South,
	West
}

struct Facing<'a> {
	front: Direction,
	cycle: Cycle<Iter<'a, Direction>>,
}

impl<'a> Facing<'a> {
	fn new(direction: Direction) -> Facing<'a> {
		let mut cycle: Cycle<Iter<Direction>> = [
			Direction::North,
			Direction::East,
			Direction::South,
			Direction::West
		].iter().cycle();

		while cycle.next() != Some(&direction) { }

		Facing { 
			front: direction,
			cycle: cycle,
		}
	}

	fn turn_left(&mut self, degrees: i32) {
		self.turn_right(360-degrees);
	}

	fn turn_right(&mut self, degrees: i32) {
		let num_of_increments = degrees / 90;
		for _ in 0..num_of_increments {
			self.front = *self.cycle.next().unwrap();
		}
	}
}


enum Action {
	Shift(Direction, i32),
	TurnLeft(i32),
	TurnRight(i32),
	Forward(i32),
}


struct Ship<'a> {
	north_cord: i32, 
	east_cord: i32,
	facing: Facing<'a>
}

impl<'a> Ship<'a> {
	
	fn new (facing: Direction) -> Ship<'a> {
		Ship { north_cord: 0, east_cord: 0, facing: Facing::new(facing) }
	}

	fn execute(&mut self, action: Action) {
		match action {
			Action::Shift(dir, amount) => {
				match dir {
					Direction::North => {
						self.north_cord += amount
					},
					Direction::South => {
						self.north_cord -= amount
					},
					Direction::East => {
						self.east_cord += amount
					},
					Direction::West => {
						self.east_cord -= amount
					},
				}
			},
			Action::TurnLeft(deg) => {
				self.facing.turn_left(deg)
			},
			Action::TurnRight(deg) => {
				self.facing.turn_right(deg)
			},
			Action::Forward(amount) => {
				match self.facing.front {
					Direction::North => {
						self.north_cord += amount
					},
					Direction::South => {
						self.north_cord -= amount
					},
					Direction::East => {
						self.east_cord += amount
					},
					Direction::West => {
						self.east_cord -= amount
					},
				}
			}
		}

	}
}



fn main() {
    let file = File::open("input.txt").expect("could not open file");
    let content = BufReader::new(file);
    let mut ship = Ship::new(Direction::East);

    for line in content.lines() {
    	let line = line.unwrap();

    	let mut char_iter = line.chars();
    	let action_char = char_iter.next().unwrap();
    	let amount_str: String = char_iter.collect();
    	let amount = amount_str.parse::<i32>().unwrap();

    	let action = match action_char {
			'N' => Action::Shift(Direction::North, amount),
			'S' => Action::Shift(Direction::South, amount),
			'E' => Action::Shift(Direction::East, amount),
			'W' => Action::Shift(Direction::West, amount),
			'L' => Action::TurnLeft(amount),
			'R' => Action::TurnRight(amount),
			'F' => Action::Forward(amount),
			x => panic!("Unknown character action {}", x)
    	};

    	ship.execute(action);
    }

    println!("Ships cordinates {}, {}", ship.north_cord, ship.east_cord);
    println!("Blocks away: {}", 
    	(if ship.north_cord > 0 { ship.north_cord } else { -1*ship.north_cord }) +
    	(if ship.east_cord > 0 { ship.east_cord } else { -1*ship.east_cord })
	);

}

