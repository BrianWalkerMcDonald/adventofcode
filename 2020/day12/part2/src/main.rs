use std::fs::File;
use std::io::{BufReader, BufRead};

#[derive(PartialEq, Clone, Copy)]
enum Direction {
	North(i32),
	East(i32),
	South(i32),
	West(i32)
}

#[derive(PartialEq, Clone, Copy)]
enum Rotate {
	Left(i32),
	Right(i32),
}

enum Action {
	Shift(Direction),
	Turn(Rotate),
	Forward(i32),
}

struct Waypoint {
	cordinate_north: i32,
	cordinate_east:  i32,
}

impl Waypoint {

	fn shift(&mut self, direction: Direction ) {
		match direction {
			Direction::North(amount) => { self.cordinate_north += amount },
			Direction::South(amount) => { self.cordinate_north -= amount },
			Direction::East(amount)  => { self.cordinate_east  += amount },
			Direction::West(amount)  => { self.cordinate_east  -= amount },
 		}
	}

	fn rotate(&mut self, turn: Rotate) {

		match turn {
			Rotate::Left(degrees)  => { 
				let increment = degrees / 90;

				for _ in 0..increment {
					let tmp = self.cordinate_north;
					self.cordinate_north = self.cordinate_east;
					self.cordinate_east = tmp;
					self.cordinate_east *= -1;
				}
			},
			Rotate::Right(degrees) => {  
				let increment = degrees / 90;

				for _ in 0..increment {
					let tmp = self.cordinate_north;
					self.cordinate_north = self.cordinate_east;
					self.cordinate_east = tmp;
					self.cordinate_north *= -1;
				}
			},
		}
	}
}

struct Ship {
	position_north: i32,
	position_east:  i32,
	waypoint: Waypoint,
}

impl Ship {

	fn new (cordinate_north: i32, cordinate_east: i32) -> Ship {
		Ship {
			position_north: 0,
			position_east: 0,
			waypoint: Waypoint { cordinate_north, cordinate_east },
		}
	}

	fn forward(&mut self, amount: i32) {
		for _ in 0..amount {
			self.position_east += self.waypoint.cordinate_east;
			self.position_north += self.waypoint.cordinate_north;
		}
		
	}

	fn execute(&mut self, action: Action) {
		match action {
			Action::Shift(direction) => self.waypoint.shift(direction),
			Action::Turn(rotate) => self.waypoint.rotate(rotate),
			Action::Forward(amount) => self.forward(amount),
		}
	}

}


fn main() {
	let file = File::open("input.txt").expect("could not open file");
    let content = BufReader::new(file);
    let mut ship = Ship::new(1, 10);

    for line in content.lines() {
    	let line = line.unwrap();

    	let mut char_iter = line.chars();
    	let action_char = char_iter.next().unwrap();
    	let amount_str: String = char_iter.collect();
    	let amount = amount_str.parse::<i32>().unwrap();

    	let action = match action_char {
			'N' => Action::Shift(Direction::North(amount)),
			'S' => Action::Shift(Direction::South(amount)),
			'E' => Action::Shift(Direction::East(amount)),
			'W' => Action::Shift(Direction::West(amount)),
			'L' => Action::Turn(Rotate::Left(amount)),
			'R' => Action::Turn(Rotate::Right(amount)),
			'F' => Action::Forward(amount),
			x => panic!("Unknown character action {}", x)
    	};

    	ship.execute(action);
    }

    println!("Ships cordinates {}, {}", ship.position_north, ship.position_east);
    println!("Blocks away: {}", ship.position_north.abs() + ship.position_east.abs());

}