use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::{ HashMap, HashSet };


fn recursive(key: &str, hash_map: &HashMap<String, HashSet<String>>, hash_set: &mut HashSet<String>) {

	let values = match hash_map.get(key) {
		None => return,
		Some(x) => x,
	};


	for value in values.iter() {
		if hash_set.contains(value) {
			continue;
		} else {
			hash_set.insert(value.clone());
			recursive(value, hash_map, hash_set);
		}
	}
}


fn main() {

	let file = File::open("input.txt").expect("Could not open file");
	let buffer = BufReader::new(file);

	let mut hash_map = HashMap::new();

	for line in buffer.lines() {
		let line = line.unwrap();
		if line.ends_with("no other bags.") {
			continue;
		}

		let line_split: Vec<&str> = line.split(' ').collect();

		match &line_split[..] {
			[adverb, adjective, "bags", "contain", other @ ..] => {
				for chunk in other.chunks_exact(4) {
					// 1 bright white bag,
					let sub_adverb = chunk[1];
					let sub_adjective = chunk[2];

					let parents = hash_map.entry(format!("{} {}", sub_adverb, sub_adjective)).or_insert(HashSet::new());
					parents.insert(format!("{} {}", adverb, adjective));
				}
			},
			
			_ => { panic!("Unpack error"); }

		};


	}

	let my_bag = "shiny gold";
	let mut my_hash_set = HashSet::new();
	recursive(my_bag, &hash_map, &mut my_hash_set);

	println!("The number of parents is {}", my_hash_set.len());
}
