use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::{ HashMap };


fn recursive(key: &str, hash_map: &HashMap<String, Vec<(u32, String)>>) -> u32 {

	match hash_map.get(key) {
		None => 1,
		Some(values) => {
			values.iter().fold(1, | acc, ( number_of_bags, bag_id ) | {
					acc + number_of_bags * recursive(bag_id, hash_map)
			})
		}
	}
}


fn main() {

	let file = File::open("input.txt").expect("Could not open file");
	let buffer = BufReader::new(file);

	let mut hash_map = HashMap::new();

	for line in buffer.lines() {
		let line = line.unwrap();

		let line_split: Vec<&str> = line.split(' ').collect();

		match &line_split[..] {
			[adverb, adjective, "bags", "contain", "no", "other", "bags"] => {
				hash_map.entry(format!("{} {}", adverb, adjective)).or_insert(Vec::new());
			}
			[adverb, adjective, "bags", "contain", other @ ..] => {
				let children = hash_map.entry(format!("{} {}", adverb, adjective)).or_insert(Vec::new());

				for chunk in other.chunks_exact(4) {
					// 1 bright white bag,
					let number = chunk[0].parse::<u32>().unwrap();
					let sub_adverb = chunk[1];
					let sub_adjective = chunk[2];

					children.push((number, format!("{} {}", sub_adverb, sub_adjective)));
				}
			},
			
			_ => { panic!("Unpack error"); }

		};


	}

	let my_bag = "shiny gold";
	let number_of_bags = recursive(my_bag, &hash_map);

	// Not sure on the off by one error here;
	println!("The {} bag needs to hold {} bags", my_bag, number_of_bags - 1);
}
