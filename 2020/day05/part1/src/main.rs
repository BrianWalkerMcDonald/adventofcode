use std::fs::File;
use std::io::{BufRead, BufReader};


fn main() {
    let file = File::open("input.txt").expect("Unable to open file");
    let f = BufReader::new(file);

    let max_id = f
    	.lines()
		.map(|line| line.unwrap())
		.map(|line| {
			let row_str: &str = &line[..7];
			let row: u32 = row_str.chars()
				.fold(0, | acc, c | { ( acc << 1 ) + ( (c == 'B') as u32) });

			let col_str: &str = &line[7..];
			let col: u32 = col_str.chars()
				.fold(0, | acc, c | { ( acc << 1 ) + ( (c == 'R') as u32) });
				
			8 * row + col
		})
		.max();

	println!("The max id is {}", max_id.unwrap());
}
