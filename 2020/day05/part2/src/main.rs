use std::fs::File;
use std::io::{BufRead, BufReader};


fn main() {
    let file = File::open("input.txt").expect("Unable to open file");
    let f = BufReader::new(file);

    let mut seat_tracker: [bool; 128 * 8] = [false; 128 * 8];

    f.lines()
		.map(|line| line.unwrap())
		.map(|line| {
			let row_str: &str = &line[..7];
			let row: u32 = row_str.chars()
				.fold(0, | acc, c | { ( acc << 1 ) + ( (c == 'B') as u32) });

			let col_str: &str = &line[7..];
			let col: u32 = col_str.chars()
				.fold(0, | acc, c | { ( acc << 1 ) + ( (c == 'R') as u32) });
			
			let seat_id = row * 8 + col;

			seat_tracker[seat_id as usize] = true;
		})
		// exhaust
		.for_each(|_| ());


	// assuming missing seats are continous in the front and back
	let result = seat_tracker
		.iter()
		.enumerate()
		// skip missing seats
		.skip_while(|(_, x)| !(**x))
		// skip taken seats
		.skip_while(|(_, x)| **x)
		.next();
		
	let my_seat = match result {
		Some((x, _)) => x,
		None => { panic!("Oh None"); },
	};

	println!("My seat {}", my_seat);
}
