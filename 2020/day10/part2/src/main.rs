use std::fs::File;
use std::io::{ BufReader, BufRead };
use std::collections::VecDeque;

fn main() {
    
	let file = File::open("input.txt").expect("File could not open");
	let file_buffer = BufReader::new(file);

	let mut joltage_adapters: Vec<u64> = file_buffer.lines()
		.map(| line | line.unwrap())
		.map(| line | line.parse::<u64>().unwrap() )
		.collect();

	joltage_adapters.sort();


	let mut window: VecDeque<u64> = VecDeque::with_capacity(3);
	window.push_back(0);
	window.push_back(0);
	window.push_back(1);

	let mut previous_adapter = &0;
	println!("{} - {:?}, {}", 0, window, window.back().unwrap());

	for adapter in joltage_adapters.iter() {
		let difference = adapter - previous_adapter;

		match difference {
			3 => { 
				window.pop_front();
				window.push_back(0);
				window.pop_front();
				window.push_back(0);
			}
			2 => {
				window.pop_front();
				window.push_back(0);
			}
			1 => {

			}
			_ => {
				panic!("A jump of {}", difference);
			}

		}


		let number_of_branches = window.iter().sum::<u64>();

		window.pop_front();
		window.push_back(number_of_branches);

		println!("{} - {:?}, {}", adapter, window, window.back().unwrap());
		previous_adapter = adapter;
	}


	println!("{} - {:?}, {}", previous_adapter + 3, window, window.back().unwrap());

	println!("Total branches {}", window.back().unwrap());
}
