use std::fs::File;
use std::io::{ BufReader, BufRead };

fn main() {
    
	let file = File::open("input.txt").expect("File could not open");
	let file_buffer = BufReader::new(file);

	let mut joltage_adapters: Vec<u32> = file_buffer.lines()
		.map(| line | line.unwrap())
		.map(| line | line.parse::<u32>().unwrap() )
		.collect();

	let mut joltage_distribution = [0, 0, 0];
	joltage_adapters.sort();
	let joltage_adapter_iter = joltage_adapters.iter();

	let mut previous_adapter: &u32 = &0;
	for adapter in joltage_adapter_iter {
		joltage_distribution[(adapter - previous_adapter - 1) as usize] += 1;
		previous_adapter = adapter;
	}

	joltage_distribution[2] += 1;

	println!("jolt-1 {}, jolt-2 {}, jolt-3 {}", joltage_distribution[0], joltage_distribution[1], joltage_distribution[2]);
	println!("Mulitplied {}", joltage_distribution[0] * joltage_distribution[2]);
}

