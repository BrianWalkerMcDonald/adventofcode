use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::VecDeque;


struct XmasEncyrption {
	buffer: VecDeque<u32>,
	preambel: usize,
}

impl XmasEncyrption {

	fn new(preambel: usize) -> XmasEncyrption {
		XmasEncyrption { buffer: VecDeque::with_capacity(preambel), preambel }
	}

	fn feed_number(&mut self, input_number: u32) -> bool {
		if self.buffer.len() < self.preambel {
			self.buffer.push_back(input_number);
			true
		} else {
			let result = self.validate_number(input_number);
			self.buffer.pop_front();
			self.buffer.push_back(input_number);
			result
		}
	}


	fn validate_number(&self, input_number: u32) -> bool {
		for val_one in self.buffer.iter() {
			for val_two in self.buffer.iter() {
				if val_one + val_two == input_number {
					return true;
				};
			}
		}

		return false;
	}
}


fn main() {
    let file = File::open("input.txt").expect("Could not open file");
    let file_buffer = BufReader::new(file);

    let mut xmas_encyrption = XmasEncyrption::new(25);

    for line_wrapped in file_buffer.lines() {
    	let line = line_wrapped.unwrap();
    	let value = line.parse::<u32>().unwrap();

    	let validation = xmas_encyrption.feed_number(value);
    	if !validation {
    		println!("The invalid number was {}", value);
    		return;
    	}
    }
    println!("No invalid elements found");
}
