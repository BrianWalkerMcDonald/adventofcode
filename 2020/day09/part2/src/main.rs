use std::fs::File;
use std::io::{BufReader, BufRead};
use std::collections::VecDeque;
use std::cmp::Ordering;

struct XmasEncyrption {
	buffer: VecDeque<u64>,
	preambel: usize,
}

impl XmasEncyrption {

	fn new(preambel: usize) -> XmasEncyrption {
		XmasEncyrption { buffer: VecDeque::with_capacity(preambel), preambel }
	}

	fn feed_number(&mut self, input_number: u64) -> bool {
		if self.buffer.len() < self.preambel {
			self.buffer.push_back(input_number);
			true
		} else {
			let result = self.validate_number(input_number);
			self.buffer.pop_front();
			self.buffer.push_back(input_number);
			result
		}
	}


	fn validate_number(&self, input_number: u64) -> bool {
		for val_one in self.buffer.iter() {
			for val_two in self.buffer.iter() {
				if val_one + val_two == input_number {
					return true;
				};
			}
		}

		return false;
	}
}


fn main() {
    let file = File::open("input.txt").expect("Could not open file");
    let file_buffer = BufReader::new(file);

    let mut xmas_encyrption = XmasEncyrption::new(25);

    let values: Vec<u64> =  file_buffer.lines()
    	.map( | line | { line.unwrap() })
    	.filter( | line | line.len() > 0 )
    	.map( | line | { line.parse::<u64>().unwrap() })
    	.collect();


    let mut invalid_number: u64 = 0;
    for val in values.iter() {
    	let validation = xmas_encyrption.feed_number(*val);
    	if !validation {
    		println!("The invalid number was {}", val);
    		invalid_number = *val;
    		break;
    	}
    }


    let mut vec_deque: VecDeque<u64> = VecDeque::new();
    let mut total: u64 = 0;
    let mut value_iterator = values.iter();

    loop {
    	match total.cmp(&invalid_number) {
    		Ordering::Less => {
    			let val = value_iterator.next().unwrap();
    			total += val;
    			vec_deque.push_back(*val);
    		},
    		Ordering::Greater => {
    			let val = vec_deque.pop_front().unwrap();
    			total -= val;
    		},
    		Ordering::Equal => {
    			let min = vec_deque.iter().min().unwrap();
    			let max = vec_deque.iter().max().unwrap();

    			println!("{} + {} = {}", min, max, min+max);
    			break;
    		}
    	}
    }
}
