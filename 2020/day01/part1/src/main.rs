use std::fs;


fn main() {
	let filename = "input_2.txt";

	let content = fs::read_to_string(filename)
		.expect("Unable to read input.txt file");


	let mut ascending: Vec<u32> = content
		.split_whitespace()
		.map(|x| x.parse::<u32>().unwrap())
		.collect();

	ascending.sort();
	let mut descending = ascending.clone();

	descending.reverse();

	let mut ascending_iter = ascending.iter();
	let mut descending_iter = descending.iter();

	let mut a_val = ascending_iter.next().unwrap();
	let mut d_val = descending_iter.next().unwrap();
	let mut sum = a_val + d_val;
	while sum != 2020 {
		if sum > 2020 {
			d_val = descending_iter.next().unwrap();
		} else {
			a_val = ascending_iter.next().unwrap();
		}
		sum = a_val + d_val;
	}

	println!("The values were {}, {}", a_val, d_val);
	println!("The answer is {}", a_val * d_val);

}
