use std::fs;


fn search(array: &[u32], target: u32) -> Option<(&u32, &u32)> {

	let mut ascending_iter = array.iter();
	let mut descending_iter = array.iter().rev();

	let mut a_val = ascending_iter.next().unwrap();
	let mut d_val = descending_iter.next().unwrap();
	let mut sum = a_val + d_val;

	loop {
		if sum == target {
			return Some((a_val, d_val))
		} else if a_val >= d_val {
			return None
		}

		if sum > target {
			d_val = descending_iter.next().unwrap();
		} else {
			a_val = ascending_iter.next().unwrap();
		}

		sum = a_val + d_val;
	}
}

fn main() {
	let filename = "input.txt";

	let content = fs::read_to_string(filename)
		.expect("Unable to read input.txt file");


	let mut ascending: Vec<u32> = content
		.split_whitespace()
		.map(|x| x.parse::<u32>().unwrap())
		.collect();

	ascending.sort();

	for (index, val_3) in ascending.iter().enumerate() {
		let target = 2020 - val_3;
		match search(&ascending[index..], target) {
			Some((val_1, val_2)) => {
				println!("The values were {}, {}, {}", val_1, val_2, val_3);
				println!("The answer is {}", val_1 * val_2 * val_3);
				break;				
			} 
			None => {}
		}
	}
}
