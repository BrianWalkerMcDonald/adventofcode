use std::fs;


#[derive(Debug)]
struct PasswordEntry<'a> {
	index_one: usize,
	index_two: usize,
	character: char,
	password: &'a str,

}


impl<'a> PasswordEntry<'a> {
	fn from_line(input_line: &'a str) -> PasswordEntry {

		let first_split: Vec<&str> = input_line.split(' ').collect();

		let (min_max, character_colon, password) = match &first_split[..] {
			&[one, two, three] => (one, two, three),
			_ => { panic!("Unpack Error"); }
		};

		let min_max_split: Vec<&str> = min_max.split('-').collect();

		let (index_one, index_two) = match &min_max_split[..] {
			&[one, two] => (one.parse::<usize>().unwrap(), two.parse::<usize>().unwrap()),
			_ => { panic!("Min max Unpack error"); }
		};

		let character: char = character_colon.chars().nth(0).unwrap();


		PasswordEntry { index_one, index_two, character, password }
	}


	fn is_valid(&self) -> bool {
		(
			self.password.chars().nth(self.index_one - 1) == Some(self.character)
		) ^ (
			self.password.chars().nth(self.index_two - 1) == Some(self.character)
		)
	}
}


fn main() {
	let filename = "input.txt";

	let content = fs::read_to_string(filename)
		.expect("Unable to read input.txt file");

	let total_valid_passwords: u32  = content
		.trim()
		.split('\n')
		.map(|line| PasswordEntry::from_line(line))
		.map(|password_entry| password_entry.is_valid() as u32)
		.sum();

	println!("There are {} total valid passwords",  total_valid_passwords);
}

