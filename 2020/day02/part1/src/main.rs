use std::fs;


#[derive(Debug)]
struct PasswordEntry<'a> {
	min_value: u32,
	max_value: u32,
	character: char,
	password: &'a str,

}


impl<'a> PasswordEntry<'a> {
	fn from_line(input_line: &'a str) -> PasswordEntry {

		let first_split: Vec<&str> = input_line.split(' ').collect();

		let (min_max, character_colon, password) = match &first_split[..] {
			&[one, two, three] => (one, two, three),
			_ => { panic!("Unpack Error"); }
		};

		let min_max_split: Vec<&str> = min_max.split('-').collect();

		let (min_value, max_value) = match &min_max_split[..] {
			&[one, two] => (one.parse::<u32>().unwrap(), two.parse::<u32>().unwrap()),
			_ => { panic!("Min max Unpack error"); }
		};

		let character: char = character_colon.chars().nth(0).unwrap();


		PasswordEntry { min_value, max_value, character, password }
	}


	fn is_valid(&self) -> bool {
		let char_count = self.password.chars().map(|c| (c == self.character) as u32 ).sum();
		self.min_value <= char_count && self.max_value >= char_count
	}
}


fn main() {
	let filename = "input.txt";

	let content = fs::read_to_string(filename)
		.expect("Unable to read input.txt file");

	let total_valid_passwords: u32  = content
		.trim()
		.split('\n')
		.map(|line| PasswordEntry::from_line(line))
		.map(|password_entry| password_entry.is_valid() as u32)
		.sum();

	println!("There are {} total valid passwords",  total_valid_passwords);
}

