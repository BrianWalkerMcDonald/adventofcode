use std::io::{BufRead, BufReader};
use std::fs::File;
use std::str;


fn validate_year(value: &str, start_year: u32, end_year: u32) -> bool {
	match value.parse::<u32>() {
		Ok(x) => x >= start_year && x <= end_year,
		Err(_) => false,
	}
}


fn validate_passport(input_string: &str) -> bool {

	if 	input_string.contains("byr:") &&
		input_string.contains("iyr:") &&
		input_string.contains("eyr:") &&
		input_string.contains("hgt:") &&
		input_string.contains("hcl:") &&
		input_string.contains("ecl:") &&
		input_string.contains("pid:") {
 
		input_string
			.split_whitespace()
			.map( | chunk | {

				let first_split: Vec<&str> = chunk.split(':') .collect();
				match &first_split[..] {
					&[key, value] => (key, value),
					_ => { panic!("Unpack Error"); }
				}
			})
			.map( | (key, value) | {
				match key {
					"byr" => validate_year(value, 1920, 2002),
					"iyr" => validate_year(value, 2010, 2020),
					"eyr" => validate_year(value, 2020, 2030),

					"hgt" => {

						if value.ends_with("in") {
							match value[..value.len()-2].parse::<u32>() {
								Ok(x)  => x >= 59 && x <= 76,
								Err(_) => false
							}
						} else if value.ends_with("cm")  {
							match value[..value.len()-2].parse::<u32>() {
								Ok(x)  => x >= 150 && x <= 193,
								Err(_) => false
							}
						} else {
							false
						}
					}

					"hcl" => {
						value.starts_with("#") && value[1..].chars().all( | c | c.is_ascii_hexdigit() )
					}

					"ecl" =>  {
						match value {
							"amb" | "blu" | "brn" | "gry" | "grn" | "hzl" | "oth" => true,
							_ => false
						}
					}

					"pid" =>  {
						value.len() == 9 && value.chars().all( | c | c.is_numeric())
					}

					"cid" =>  { true }
					_ => {
						println!("Unknown key {}", key);
						false
					}
				}

			}).all(| x | x )

	} else {
		false
	}
}


fn main() {
	let file = File::open("input.txt").expect("open failed");
    let mut f = BufReader::new(file);

    let mut buf = vec![];
    let mut total_valid = 0;

    loop {
    	let read_len = f.read_until(b'\n', &mut buf).expect("Unable to read file");

    	match read_len {
    		0 => {
				let passport_string = str::from_utf8(&buf).expect("Unable to turn buffer into string");
    			if validate_passport(passport_string) {
    				total_valid += 1;
    			};
    			break	
    		},  // EOF
    		1 => { // assume b'\n'
    			let passport_string = str::from_utf8(&buf).expect("Unable to turn buffer into string");
    			if validate_passport(passport_string) {
    				total_valid += 1;
    			};
    			buf.clear();
    		},
    		_ => continue, // Carry on grabbing bytes
    	};
    }

    println!("The number of valid passports is {}", total_valid);
}
