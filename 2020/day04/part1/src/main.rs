use std::io::{BufRead, BufReader};
use std::fs::File;
use std::str;


fn validate_passport(input_string: &str) -> bool {
    input_string.contains("byr:") &&
    input_string.contains("iyr:") &&
    input_string.contains("eyr:") &&
    input_string.contains("hgt:") &&
    input_string.contains("hcl:") &&
    input_string.contains("ecl:") &&
    input_string.contains("pid:")
}


fn main() {
    let file = File::open("input.txt").expect("open failed");
    let mut f = BufReader::new(file);

    let mut buf = vec![];
    let mut total_valid = 0;

    loop {
        let read_len = f.read_until(b'\n', &mut buf).expect("Unable to read file");

        match read_len {
            0 => {
                let passport_string = str::from_utf8(&buf).expect("Unable to turn buffer into string");
                if validate_passport(passport_string) {
                    total_valid += 1;
                };
                break   
            },  // EOF
            1 => { // assume b'\n'
                let passport_string = str::from_utf8(&buf).expect("Unable to turn buffer into string");
                if validate_passport(passport_string) {
                    total_valid += 1;
                };
                buf.clear();
            },
            _ => continue, // Carry on grabbing bytes
        };
    }

    println!("The number of valid passports is {}", total_valid);
}
