use std::fs::File;
use std::io::{BufReader, BufRead};
use std::iter::FlatMap;
use std::fmt;


#[derive(Copy, Clone, PartialEq)]
enum Cell {
	Floor,
	SeatEmpty,
	SeatTaken
}


struct XmasGameOfLife {
	previous_data: Vec<Cell>,
	next_data: Vec<Cell>,
	width: usize,
	height: usize,
}

impl XmasGameOfLife {

	fn from_file(input_filename: &str) ->  XmasGameOfLife {
		
		let file = File::open(input_filename).expect("Can not open file");
    	let mut content = BufReader::new(file);

    	let mut line = String::new();
    	let width = content.read_line(&mut line);
    	let width = width.unwrap() - 1;
    	let mut height = 1;

    	let line = line.trim();

    	let mut data = Vec::new();

    	data.extend(
    		line.chars().map( | c |
   				match c {
	   					'#' => Cell::SeatTaken,
	   					'.' => Cell::Floor,
	   					'L' => Cell::SeatEmpty,
	   					_ => { panic!("Unknown character {}", c);}
   				}
			)
    	);

    	for line_wraped in content.lines() {

	   		let line = line_wraped.unwrap();
	   		let line = line.trim();

	   		if line.len() == 0 {
	   			continue;
	   		} else {
    			height += 1;
	   		}

	   		data.extend(
	   			line.chars()
			   		.map(|c|
		   				match c {
		   					'#' => Cell::SeatTaken,
		   					'.' => Cell::Floor,
		   					'L' => Cell::SeatEmpty,
		   					_ => { panic!("Unknown character {}", c);}
		   				}
					)
			)
    	}



		XmasGameOfLife {
			next_data: vec![Cell::Floor; data.len()],
			previous_data: data,
			height,
			width,
		}
	}


	fn get_index(&self, row_index: usize, col_index: usize) -> usize{
		row_index * self.width + col_index
	}

	fn get_neighbors(&self, row_index: usize, col_index: usize) -> Vec<Cell>{

		let mut neighbors: Vec<Cell> = Vec::new();
		let deltas: Vec<(i32, i32)> = vec![ 
			(-1, -1),
			(0, -1),
			(1, -1),
			(-1, 0),
			(1, 0),
			(-1, 1),
			(0, 1),
			(1, 1),
		];

		for (dx, dy) in deltas.iter() {
			if *dx == -1 && row_index == 0 {
				continue;
			} else if *dx == 1 && row_index == self.height - 1 {
				continue;
			}
			if *dy == -1 && col_index == 0 {
				continue;
			} else if *dy == 1 && col_index == self.width - 1 {
				continue;
			}

			let x_id = (row_index as i32) + dx;
			let y_id = (col_index as i32) + dy;
			let cell = self.previous_data[self.get_index(x_id as usize, y_id as usize)];
			neighbors.push(cell);
		}

		neighbors
	}


	fn iterate_row_col(&self) -> Vec<(usize, usize)> {
		(0..self.height).flat_map( 
			| r | (0..self.width).map( move | c | (r, c))
		).collect()
	}


	fn update(&mut self) {
		for (row, col) in self.iterate_row_col() {
			let current_index = self.get_index(row, col);
			let cell = match self.previous_data.get(current_index){
				Some(x) => x,
				None => panic!("current index None: {}, {}, {}, {}, {}", row, col, current_index, self.width, self.height)
			};


			let neighbors = self.get_neighbors(row, col);

			let new_cell = match cell {
				Cell::Floor => { continue; },
				Cell::SeatTaken => {
					if neighbors.iter().map( | seat | (*seat == Cell::SeatTaken) as u8 ).sum::<u8>() >= 4 {
						Cell::SeatEmpty
					} else {
						Cell::SeatTaken
					}
				},
				Cell::SeatEmpty => {
					if neighbors.iter().any( | seat | *seat == Cell::SeatTaken ) {
						Cell::SeatEmpty
					} else {
						Cell::SeatTaken
					}
				},
			};

			self.next_data[current_index] = new_cell;
		}

		let tmp_data = self.next_data.clone();
		self.next_data = self.previous_data.clone();
		self.previous_data = tmp_data;
	}

}

impl fmt::Display for XmasGameOfLife {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {

        for line in self.previous_data.as_slice().chunks(self.width as usize) {
            for &cell in line {
                let symbol = match cell {
   					Cell::SeatTaken => '#',
   					Cell::Floor => '.',
   					Cell::SeatEmpty => 'L',
                };
                write!(f, "{}", symbol)?;
            }
            write!(f, "\n")?;
        }

        Ok(())
    }
}



fn main() {
    let mut xgol = XmasGameOfLife::from_file("input.txt");

    while xgol.previous_data != xgol.next_data {
    	xgol.update();
    }
	println!("{}\n", xgol);
	println!("Number of occupied seats: {}", 
		xgol.previous_data.iter()
			.map(| seat | (*seat == Cell::SeatTaken) as u32)
			.sum::<u32>()
	);
}