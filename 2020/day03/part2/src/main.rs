use std::io::{BufRead, BufReader};
use std::fs::File;



struct SlopeTracker {
	right: usize,
	down:  u32,
	column_index: usize,
	row_counter: u32,
	total: u32
}

impl SlopeTracker {

	fn new(right: usize, down: u32) -> SlopeTracker {
		SlopeTracker { right, down, column_index: 0, row_counter: down, total: 0 }
	}

	fn feed_line(&mut self, line: &String) {
		if self.row_counter == self.down {
			if self.column_index >= line.len() {
				self.column_index -= line.len();
			}
			self.total += (line.chars().nth(self.column_index) == Some('#')) as u32;

			self.column_index += self.right;
			self.row_counter = 0;
		}
		self.row_counter += 1;
	}

}



fn main() {
	let file = File::open("input.txt").expect("open failed");
    let f = BufReader::new(file);

    let slope_tracker_list = &mut [ 
    	SlopeTracker::new(1, 1),
    	SlopeTracker::new(3, 1),
    	SlopeTracker::new(5, 1),
    	SlopeTracker::new(7, 1),
    	SlopeTracker::new(1, 2),
    ];

    for line_result in f.lines() {
    	let line = line_result.unwrap();

    	for slope_tracker in slope_tracker_list.iter_mut() {
    		slope_tracker.feed_line(&line);
    	}
    }

    for slope_tracker in slope_tracker_list.iter() {
	    println!("Total number of trees: {}", slope_tracker.total);
    }

    println!("The multiple is {}", slope_tracker_list.iter().fold(1, |acc, x| acc * x.total));
}
