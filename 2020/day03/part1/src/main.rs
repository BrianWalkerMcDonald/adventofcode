use std::io::{BufRead, BufReader};
use std::fs::File;


fn main() {
	let file = File::open("input.txt").expect("open failed");
    let f = BufReader::new(file);

    let mut index = 0;

    let tree_total: u32 = f.lines()
    					 .map(|line| line.unwrap())
					     .map(|line| {
					     	if index >= line.len() {
					     		index -= line.len();
					     	}
					     	let answer = line.chars().nth(index) == Some('#');
					     	index += 3;
					     	answer as u32
					     }).sum();

    println!("Total number of trees: {}", tree_total);
}
