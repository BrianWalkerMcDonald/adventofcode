use std::fs::File;
use std::io::{BufReader, Bytes};
use std::io::prelude::*;

use std::collections::HashSet;


struct GroupAnswersCounter<B> {
	byte_iterator: Bytes<B>,
	answer_buffer: HashSet<u8>,
	previous_end: bool,
	eof: bool,
}

impl<B> GroupAnswersCounter<B>{

	fn new(byte_iterator: Bytes<B>) -> GroupAnswersCounter<B> {

		GroupAnswersCounter {
			byte_iterator,
			previous_end: false,
			eof: false,
			answer_buffer: HashSet::new()
		}

	}

	fn group_count(&mut self) -> Option<u32> {

		let result = self.answer_buffer.len();
		self.answer_buffer.clear();

		return Some(result as u32);

	}
}


impl<B: Read> Iterator for GroupAnswersCounter<B> {
    type Item = u32;


    fn next(&mut self) -> Option<Self::Item> {
    	loop {
    		let next_byte = self.byte_iterator.next();
    		

    		match next_byte {
    			Some(Err(x)) => panic!("{:?}", x),
    			Some(Ok(b'\n')) => {
    				if self.previous_end {
	    				return self.group_count();
	    			} else {
	    				self.previous_end = true;
	    			}
    			},
    			None => {
	    			if self.eof {
	    				return None
	    			} else {
	    				self.eof = true;
	    				return self.group_count();
					}    				
    			},
    			Some(Ok(byte)) => {
		    		self.previous_end = false;
		    		self.answer_buffer.insert(byte);    				
    			},
    		}
    	}
    	
    }
}


fn main() {
    
    let file = File::open("input.txt").expect("Can not open file");
    let reader = BufReader::new(file);
        
    let answer_counter = GroupAnswersCounter::new(reader.bytes());

    let answer: u32 = answer_counter.sum();

    println!("The sum of the yes group answers is {}", answer);
}
