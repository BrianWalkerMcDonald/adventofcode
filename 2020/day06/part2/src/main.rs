use std::fs::File;
use std::io::{BufReader, Bytes};
use std::io::prelude::*;

use std::collections::HashSet;


struct GroupAnswersCounter<B> {
	byte_iterator: Bytes<B>,
	previous_buffer: HashSet<u8>,
	next_buffer: HashSet<u8>,
	previous_newline: bool,
	first_line_of_group: bool,
	eof: bool,
}

impl<B> GroupAnswersCounter<B>{

	fn new(byte_iterator: Bytes<B>) -> GroupAnswersCounter<B> {

		GroupAnswersCounter {
			byte_iterator,
			previous_newline: false,
			first_line_of_group: true,
			eof: false,
			previous_buffer: HashSet::new(),
			next_buffer: HashSet::new()
		}

	}

	fn group_count(&mut self) -> Option<u32> {

		let result = self.previous_buffer.len();
		self.previous_buffer.clear();
		self.next_buffer.clear();
		self.first_line_of_group = true;
		return Some(result as u32);

	}
}


impl<B: Read> Iterator for GroupAnswersCounter<B> {
    type Item = u32;


    fn next(&mut self) -> Option<Self::Item> {
    	loop {
    		let next_byte = self.byte_iterator.next();
    		

    		match next_byte {
    			Some(Err(x)) => panic!("{:?}", x),

    			Some(Ok(b'\n')) => {
    				// the double enter
    				if self.previous_newline {

	    				return self.group_count();

	    			// previous line was first in the group
	    			} else if self.first_line_of_group {

	    				self.first_line_of_group = false;
	    				self.previous_newline = true;

	    			// previous line was first in the group
	    			} else {

	    				self.previous_buffer = self.previous_buffer.intersection(&self.next_buffer).map(|c| *c).collect();
	    				self.next_buffer.clear();
	    				self.previous_newline = true;
	    			}
    			},
    			None => {
	    			if self.eof {
	    				return None
	    			} else {
	    				self.eof = true;
	    				return self.group_count();
					}    				
    			},
    			Some(Ok(byte)) => {
    				if self.first_line_of_group {
    					self.previous_buffer.insert(byte);
    				} else {
    					self.next_buffer.insert(byte);
    				}
		    		self.previous_newline = false;
    			},
    		}
    	}
    	
    }
}


fn main() {
    
    let file = File::open("input.txt").expect("Can not open file");
    let reader = BufReader::new(file);
        
    let answer_counter = GroupAnswersCounter::new(reader.bytes());

    let answer: u32 = answer_counter.sum();

    println!("The sum of the yes group answers is {}", answer);
}
