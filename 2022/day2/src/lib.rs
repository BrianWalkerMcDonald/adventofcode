use std::path::Path;
use std::fs::File;
use std::io;
use common::AdventDay;

enum GameScore {
    Tie,
    Lose,
    Win,
}

impl GameScore {
    fn value(&self) -> u32 {
        match self {
            GameScore::Lose => { 0 }
            GameScore::Tie  => { 3 }
            GameScore::Win  => { 6 }
        }
    }
}


enum RPS {
    Rock,
    Paper,
    Scissors,
}

impl RPS {
    fn value(&self) -> u32 {
        match self {
            RPS::Rock     => { 1 }
            RPS::Paper    => { 2 }
            RPS::Scissors => { 3 }
        }
    }

    fn plays(&self, other: &RPS) -> GameScore {
        match self {
            RPS::Rock     => { 
                match other {
                    RPS::Rock     => { GameScore::Tie }
                    RPS::Paper    => { GameScore::Lose }
                    RPS::Scissors => { GameScore::Win }
                }
            }
            RPS::Paper    => {
                match other {
                    RPS::Rock     => { GameScore::Win }
                    RPS::Paper    => { GameScore::Tie }
                    RPS::Scissors => { GameScore::Lose }
                }
            }
            RPS::Scissors => {
                match other {
                    RPS::Rock     => { GameScore::Lose }
                    RPS::Paper    => { GameScore::Win }
                    RPS::Scissors => { GameScore::Tie }
                }
            }
        }
    }

    fn throw_to(&self, outcome: &GameScore) -> RPS {
        match self {
            RPS::Rock     => { 
                match outcome {
                    GameScore::Tie  => { RPS::Rock } 
                    GameScore::Win  => { RPS::Paper } 
                    GameScore::Lose => { RPS::Scissors } 
                }
            }
            RPS::Paper    => {
                match outcome {
                    GameScore::Lose => { RPS::Rock } 
                    GameScore::Tie  => { RPS::Paper } 
                    GameScore::Win  => { RPS::Scissors } 
                }
            }
            RPS::Scissors => {
                match outcome {
                    GameScore::Win  => { RPS::Rock } 
                    GameScore::Lose => { RPS::Paper } 
                    GameScore::Tie  => { RPS::Scissors } 
                }
            }
        }
    }
}

struct EncryptionOne<R> {
    data: R,
}

impl EncryptionOne<io::BufReader<File>> {

    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { data: io::BufReader::new(file) })
    }

}

impl<R: io::BufRead> Iterator for EncryptionOne<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {

        let mut buf = String::new();
        let bytes = self.data.read_line(&mut buf).unwrap();

        if bytes == 0 {
            None
        } else {
            let mut strat = buf.trim().split(" ");

            let their_move = match strat.next().unwrap() {
                "A" => RPS::Rock,
                "B" => RPS::Paper,
                "C" => RPS::Scissors,
                x => panic!("Unknown opponent move {}", x)
            };

            let my_move = match strat.next().unwrap() {
                "X" => RPS::Rock,
                "Y" => RPS::Paper,
                "Z" => RPS::Scissors,
                x => panic!("Unknown our move {}", x)
            };

            if strat.next().is_some() {
                panic!("Something went wrong in parsing");
            };

            let game_outcome = my_move.plays(&their_move);

            Some(game_outcome.value() + my_move.value())
        }
    }
}

struct EncryptionTwo<R> {
    data: R,
}

impl EncryptionTwo<io::BufReader<File>> {

    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { data: io::BufReader::new(file) })
    }

}

impl<R: io::BufRead> Iterator for EncryptionTwo<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {

        let mut buf = String::new();
        let bytes = self.data.read_line(&mut buf).unwrap();

        if bytes == 0 {
            None
        } else {
            let mut strat = buf.trim().split(" ");

            let their_move = match strat.next().unwrap() {
                "A" => RPS::Rock,
                "B" => RPS::Paper,
                "C" => RPS::Scissors,
                x => panic!("Unknown opponent move {}", x)
            };

            let game_outcome= match strat.next().unwrap() {
                "X" => GameScore::Lose,
                "Y" => GameScore::Tie,
                "Z" => GameScore::Win,
                x => panic!("Unknown game outcome {}", x)
            };

            if strat.next().is_some() {
                panic!("Something went wrong in parsing");
            };

            let my_move = their_move.throw_to(&game_outcome);

            Some(game_outcome.value() + my_move.value())
        }
    }
}

pub struct Day2;

impl Day2 {

    #[allow(dead_code)]
    fn example1() -> u32 {
        let encyrt_strat = EncryptionOne::from_file("data/example.txt").unwrap();

        encyrt_strat.sum()
    }

    #[allow(dead_code)]
    fn example2() -> u32 {
        let encyrt_strat = EncryptionTwo::from_file("data/example.txt").unwrap();

        encyrt_strat.sum()
    }
}

impl AdventDay<u32> for Day2 {
    fn part1() -> u32 {
        let encyrt_strat = EncryptionOne::from_file("day2/data/input.txt").unwrap();
        encyrt_strat.sum()
    }

    fn part2() -> u32 {
        let encyrt_strat = EncryptionTwo::from_file("day2/data/input.txt").unwrap();
        encyrt_strat.sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_encrption_one_iter() {
        let cursor = io::Cursor::new(b"A Y\nB X\nC Z\n");

        let mut strat_iter = EncryptionOne{ data: cursor };

        let first_iteration = strat_iter.next().unwrap();
        assert_eq!(first_iteration, 8);

        let second_iteration = strat_iter.next().unwrap();
        assert_eq!(second_iteration, 1);

        let third_iteration = strat_iter.next().unwrap();
        assert_eq!(third_iteration, 6);

        let empty_iteration = strat_iter.next();
        assert!(empty_iteration.is_none())
    }

    #[test]
    fn test_encrption_two_iter() {
        let cursor = io::Cursor::new(b"A Y\nB X\nC Z\n");

        let mut strat_iter = EncryptionTwo{ data: cursor };

        let first_iteration = strat_iter.next().unwrap();
        assert_eq!(first_iteration, 4);

        let second_iteration = strat_iter.next().unwrap();
        assert_eq!(second_iteration, 1);

        let third_iteration = strat_iter.next().unwrap();
        assert_eq!(third_iteration, 7);

        let empty_iteration = strat_iter.next();
        assert!(empty_iteration.is_none())
    }

    #[test]
    fn example1() {
        assert_eq!(Day2::example1(), 15);
    }

    #[test]
    fn example2() {
        assert_eq!(Day2::example2(), 12);
    }
}
