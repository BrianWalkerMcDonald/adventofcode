use std::fs::File;
use std::io;
use std::path::Path;
use common::AdventDay;

struct ElfOrders {
    start: u32,
    stop: u32,
}

impl ElfOrders {
    fn full_overlap(&self, other: &ElfOrders) -> bool {
        ( self.start <= other.start && self.stop >= other.stop ) ||
        ( self.start >= other.start && self.stop <= other.stop ) 
    }

    fn partial_overlap(&self, othr: &ElfOrders) -> bool {
        ( self.start >= othr.start && self.start <= othr.stop ) ||
        ( self.stop  >= othr.start && self.stop  <= othr.stop ) ||
        ( othr.start >= self.start && othr.start <= self.stop ) ||
        ( othr.stop  >= self.start && othr.stop  <= self.stop )
    }
}

struct DataReader<R> {
    data: R,
}

impl DataReader<io::BufReader<File>> {
    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self {
            data: io::BufReader::new(file),
        })
    }
}

impl<R: io::BufRead> Iterator for DataReader<R> {
    type Item = (ElfOrders, ElfOrders);

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = String::new();
        let bytes = self.data.read_line(&mut buf).unwrap();

        if bytes == 0 {
            return None
        }

        let parsed_line = buf.split(&['-', ','])
            .take(4)
            .map( | x | x.to_string().trim().parse::<u32>().unwrap() )
            .collect::<Vec<u32>>();
        
        if let [start1, stop1, start2, stop2] = parsed_line[..] {
            Some((ElfOrders { start: start1, stop: stop1 }, ElfOrders { start: start2, stop: stop2 }))
        } else {
            panic!("Da what?");
        }
    }
}


pub struct Day4;

impl AdventDay<usize> for Day4 {
    fn part1() -> usize {
        let data_reader = DataReader::from_file("day4/data/input.txt").unwrap();
        
        data_reader.filter(| (elf1, elf2) | elf1.full_overlap(elf2) ).count()
    }

    fn part2() -> usize {
        let data_reader = DataReader::from_file("day4/data/input.txt").unwrap();
        
        data_reader.filter(| (elf1, elf2) | elf1.partial_overlap(elf2) ).count()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1() {
        let data_reader = DataReader::from_file("data/example.txt").unwrap();
        let overlap_count = data_reader.filter(| (elf1, elf2) | elf1.full_overlap(elf2) ).count();

        assert_eq!(overlap_count, 2);
    }

    #[test]
    fn example2() {
        let data_reader = DataReader::from_file("data/example.txt").unwrap();
        let overlap_count = data_reader.filter(| (elf1, elf2) | elf1.partial_overlap(elf2) ).count();

        assert_eq!(overlap_count, 4);
    }
}
