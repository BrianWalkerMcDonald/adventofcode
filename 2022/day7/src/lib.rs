use std::{io::{self, BufRead}, collections::HashMap, path::Path, fs::File};

use common::AdventDay;


#[derive(Debug)]
struct MyDirectory {
    _name: String,
    files: Vec<MyFile>,
    directories: Vec<String>,
}

#[derive(Debug)]
struct MyFile {
    _name: String,
    size: u32,
}

#[derive(Debug)]
struct ListOutput {
    files: Vec<MyFile>,
    directories: Vec<String>,    
}

#[derive(Debug)]
enum Commands {
    UP,
    ROOT,
    CHANGE(String),
    LIST(ListOutput),
    NOP,
}

struct FileSytem {
    file_system: HashMap<String, MyDirectory>,
    current_directory: String,
}

impl FileSytem {
    fn root_path() -> String {
        "~".to_string()
    }

    fn new()-> Self {
        // Assuming starting at root
        FileSytem { file_system: HashMap::new(), current_directory: FileSytem::root_path()}
    }

    fn process(&mut self, command: Commands) {
        match command {
            Commands::UP => self.up(),
            Commands::ROOT => self.root(),
            Commands::CHANGE(name) => self.change(name),
            Commands::LIST(content) => self.add_content(content),
            Commands::NOP => {},
        };

    }

    fn up(&mut self) {
        self.current_directory = Path::new(&self.current_directory)
            .parent()
            .unwrap()
            .to_str()
            .unwrap()
            .to_string();
    }
    
    fn root(&mut self) {
        self.current_directory = FileSytem::root_path();
    }

    fn change(&mut self, name: String) {
        let next_dir = format!("{}/{}", self.current_directory, name);
        let directory = self.file_system.get(&self.current_directory).unwrap();
        if !directory.directories.contains(&next_dir) {
            panic!("{} doesn't contain {}", self.current_directory, next_dir);
        }

        self.current_directory = next_dir.to_string();
    }

    fn add_content(&mut self, content: ListOutput) {
        let result = self.file_system.insert(
            self.current_directory.clone(), 
            MyDirectory { 
                _name: self.current_directory.clone(), 
                files: content.files, 
                directories: content.directories
                    .iter()
                    .map( | sub | format!("{}/{}", self.current_directory, sub))
                    .collect()
            }
        );

        if result.is_some() {
            panic!("Double ls in {}", self.current_directory);
        }
    }

    fn find_directory_size(&self, name: &String) -> u32 {
        let directory = self.file_system.get(name).unwrap();
        directory.files
            .iter()
            .map(| f | f.size )
            .sum::<u32>() + 
        directory.directories
            .iter()
            .map(| d | self.find_directory_size(d))
            .sum::<u32>()
    }
}


struct DataReader {
    data_iterator: io::BufReader<File>
}

impl DataReader {
    fn new(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { 
            data_iterator: io::BufReader::new(file),
        })
    }
}

impl Iterator for DataReader {
    type Item = Commands;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = vec![];
        if self.data_iterator.read_until(b'$', &mut buf).unwrap() == 0 {
            return None;
        }

        let buf_string = String::from_utf8(buf).unwrap();
        let clean_string = buf_string.trim_matches(| c | char::is_whitespace(c) || c == '$');

        match clean_string {
            "" => Some(Commands::NOP),
            "cd /" => Some(Commands::ROOT),
            "cd .." => Some(Commands::UP),
            x if x.starts_with("cd") => {
                let name = x.trim_start_matches("cd ");
                Some(Commands::CHANGE(name.to_string()))
            },
            x if x.starts_with("ls") => {
                let output = x.trim_start_matches("ls\n");
                let mut directories = vec![];
                let mut files = vec![];
                for line in output.lines() {
                    let line = line.trim();
                    if line.starts_with("dir ") {
                        directories.push(line.trim_start_matches("dir ").to_string());
                    } else {
                        let mut iter = line.splitn(2, ' ');
                        let size = iter.next().unwrap().parse::<u32>().unwrap();
                        let name = iter.next().unwrap().to_string();
                        assert!(iter.next().is_none());                        
                        files.push(MyFile { _name: name, size })
                    }
                }
                Some(Commands::LIST(ListOutput { files, directories }))
            },
            x => panic!("Could not parse {}", x),
        }
    }
}


pub struct Day7;

impl Day7 {
    fn sum_directories_under_n(input_file: &str, n: u32) -> u32 {
        let data_reader = DataReader::new(input_file).unwrap();
        let mut file_system = FileSytem::new();

        for command in data_reader {
            // println!("{} {:?}",file_system.current_directory, command); 
            file_system.process(command);
        }

        file_system.file_system.keys()
            .map( | d | file_system.find_directory_size(d) )
            .filter( | size | size < &n )
            .sum()
    }

    fn smallest_directories_for_update(input_file: &str, update: u32) -> u32 {
        let data_reader = DataReader::new(input_file).unwrap();
        let mut file_system = FileSytem::new();

        for command in data_reader {
            // println!("{} {:?}",file_system.current_directory, command); 
            file_system.process(command);
        }

        let unused = 70000000 - file_system.find_directory_size(&FileSytem::root_path());
        let clean_space = update - unused;

        file_system.file_system.keys()
            .map( | d | file_system.find_directory_size(d) )
            .filter( | size | size > &clean_space )
            .min()
            .unwrap()
    }
}

impl AdventDay<u32> for Day7 {
    fn part1() -> u32 {
        Day7::sum_directories_under_n("day7/data/input.txt", 100000)
    }

    fn part2() -> u32 {
        Day7::smallest_directories_for_update("day7/data/input.txt", 30000000)
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1() {
        assert_eq!(Day7::sum_directories_under_n("data/example.txt", 100000), 95437)
    }

    #[test]
    fn example2() {
        assert_eq!(Day7::smallest_directories_for_update("data/example.txt", 30000000), 24933642)
    }
}
