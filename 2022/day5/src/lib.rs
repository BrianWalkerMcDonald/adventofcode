use std::collections::VecDeque;
use std::path::Path;
use std::fs::File;
use std::io;
use common::AdventDay;

type Crate = char;

#[derive(PartialEq, Eq)]
enum Cranes {
    Crane9000,
    Crane9001,
}

struct DataReader<R> 
where R: io::BufRead {
    data_iterator: R,
    stacks: Vec<VecDeque<Crate>>,
    crane_type: Cranes,
}

impl DataReader<io::BufReader<File>> {
    fn from_file(path: &str, crane_type: Cranes) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { 
            data_iterator: io::BufReader::new(file),
            stacks: vec![],
            crane_type,
        })
    }
}

impl<R> DataReader<R>
where R: io::BufRead {
    fn stack_phrase(&self) -> String {
        let mut result = String::new();
        for stack in self.stacks.iter() {
            if let Some(x) = stack.front() {
                result.push(*x);
            }
        }

        result
    }

    fn load_stack(&mut self) {
        let mut buf = String::new();
        if self.data_iterator.read_line(&mut buf).unwrap() == 0 {
            panic!("Nooooo.....");
        }

        let mut characters: Vec<char> = buf.chars().collect();
        if characters.len() % 4 > 0 {
            panic!("I did some bad counting. {} % 4 = {} != 0", characters.len() ,  characters.len() % 4 )
        }
        let number_of_stacks = characters.len() / 4;

        self.stacks = vec![VecDeque::new(); number_of_stacks];

        let mut header_read =  false;
        while !header_read {
            for stack_index in 0..number_of_stacks {
                let line_index = stack_index * 4 + 1;
                match characters.get(line_index) {
                    Some(' ') => continue,
                    Some(c) => {
                        if c.is_digit(10) {
                            header_read = true;
                            break;
                        }
                        self.stacks[stack_index].push_back(*c);
                    },
                    None => panic!("Hmmm, bad index {}", line_index),
                }
            }

            buf.clear();
            if self.data_iterator.read_line(&mut buf).unwrap() == 0 {
                panic!("Nooooo..... {:?}", buf);
            }
            characters = buf.chars().collect();
        }
    }
}

impl<R: io::BufRead> Iterator for DataReader<R> {
    type Item = String;

    fn next(&mut self) -> Option<Self::Item> {

        let mut buf = String::new();
        if self.data_iterator.read_line(&mut buf).unwrap() == 0 {
            return None
        }

        // assuming less than 10 stacks
        let characters: Vec<&str> = buf.trim().split(" ").collect();
        if characters.len() != 6 {
            panic!("Expectations not met. {:?}", buf);
        }

        let move_number = characters.get(1).unwrap().parse::<usize>().unwrap();
        let from_stack_index = characters.get(3).unwrap().parse::<usize>().unwrap() - 1;
        let to_stack_index = characters.get(5).unwrap().parse::<usize>().unwrap() - 1;

        let from_stack = self.stacks.get_mut(from_stack_index as usize).unwrap();
        let mut buffer = vec![];
        for _ in 0..move_number {
            let next_crate = from_stack.pop_front().unwrap();
            buffer.push(next_crate);
        }

        if self.crane_type == Cranes::Crane9001 {
            buffer.reverse();
        }

        let to_stack   = self.stacks.get_mut(to_stack_index as usize).unwrap();
        for c in buffer.iter() {
            to_stack.push_front(*c);
        }

        Some(self.stack_phrase())      
    }
}

pub struct Day5;

impl AdventDay<String> for Day5 {
    fn part1() -> String {
        let mut data_reader = DataReader::from_file("day5/data/input.txt", Cranes::Crane9000).unwrap();
        data_reader.load_stack();
        data_reader.last().unwrap()
    }

    fn part2() -> String {
        let mut data_reader = DataReader::from_file("day5/data/input.txt", Cranes::Crane9001).unwrap();
        data_reader.load_stack();
        data_reader.last().unwrap()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1() {
        let mut data_reader = DataReader::from_file("data/example.txt", Cranes::Crane9000).unwrap();
        data_reader.load_stack();
        let result = data_reader.last().unwrap();

        assert_eq!(result, "CMZ".to_string());
    }
}
