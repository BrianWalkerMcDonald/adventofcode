use std::{
    collections::{BTreeSet, VecDeque, HashSet}, 
    io::{self, BufRead}, 
    path::Path, 
    fs::File,
    iter, 
    hash::{Hash, Hasher},
    cmp::{Ord, Ordering},
};


use common::AdventDay;

#[derive(Debug)]
struct TreeLine {
    max_tree: Option<Tree>,
    front: Vec<Tree>,
    back_heap: BTreeSet<Tree>,
}


impl TreeLine {
    fn new() -> Self {
        TreeLine { max_tree: None, front: vec![], back_heap: BTreeSet::new() }
    }
    
    fn push(&mut self, tree: Tree) {
        // check front
        if self.max_tree.is_none() || tree > self.max_tree.unwrap() {
            println!("None, adding {:?}", tree);
            // add to count
            self.front.push(tree);
            // set new max tree
            self.max_tree = Some(tree);
            // no trees after max to consider now
            self.back_heap.clear();
        } else {

            // drop tree lower than tree_height
            self.back_heap = self.back_heap.split_off(&tree);

            println!("Split {:?}, adding {:?}", self.back_heap, tree);

            // add tree
            if self.back_heap.contains(&tree) {
                self.back_heap.remove(&tree);
            }
            self.back_heap.insert(tree);
        }
    }

    fn visible_trees(&self) -> Vec<Tree> {
        let mut result = self.front.clone();
        result.extend(self.back_heap.iter());
        result
    }
}


#[derive(Eq, Debug, Clone, Copy)]
struct Tree {
    height: u8,
    row: usize,
    col: usize,
}


impl Hash for Tree {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.row.hash(state);
        self.col.hash(state);
    }
}

impl Ord for Tree {
    fn cmp(&self, other: &Self) -> Ordering {
        self.height.cmp(&other.height)
    }
}

impl PartialOrd for Tree {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Tree {
    fn eq(&self, other: &Self) -> bool {
        self.height == other.height
    }
}


struct DataReader {
    data_iterator: io::BufReader<File>,
    row_buffer: VecDeque<Tree>,
    row_number: usize,
}

impl DataReader {
    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { 
            data_iterator: io::BufReader::new(file),
            row_buffer: VecDeque::new(),
            row_number: 0,
        })
    }
}

impl Iterator for DataReader {
    type Item = Tree;

    fn next(&mut self) -> Option<Self::Item> {

        if self.row_buffer.len() == 0 {
            let mut buf = String::new();
            if self.data_iterator.read_line(&mut buf).unwrap() == 0 {
                return None;
            }
    
            self.row_buffer = buf.trim()
                .chars()
                .map( | x | x.to_digit(10).unwrap() as u8 )
                .enumerate()
                .map( 
                    | (col, height) | 
                    Tree { height, col, row: self.row_number } 
                )
                .collect();

            self.row_number += 1;
        }

        Some(self.row_buffer.pop_front().unwrap())
    }
}

fn visible_trees_in_forest(path: &str) -> io::Result<usize> {
        let tree_reader = DataReader::from_file(path)?;
        let mut rows: Vec<TreeLine> = vec![];
        let mut cols: Vec<TreeLine> = vec![];

        for tree in tree_reader {
            if rows.len() == tree.row {
                rows.push(TreeLine::new());
            }

            let row_line = rows.get_mut(tree.row).unwrap();
            row_line.push(tree);

            if cols.len() == tree.col {
                cols.push(TreeLine::new());
            }

            let col_line = cols.get_mut(tree.col).unwrap();
            col_line.push(tree);
        }
        
        let mut visible_trees = HashSet::new();

        visible_trees.extend(rows.iter().map( | tree_line | tree_line.visible_trees() ).chain(
            cols.iter().map( | tree_line | tree_line.visible_trees() )
        ));

        Ok(visible_trees.len())
}


pub struct Day8;

impl AdventDay<usize> for Day8 {
    fn part1() -> usize {
        0
    }

    fn part2() -> usize {
        0   
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // #[test]
    // fn test_tree_line_1() {
    //     let test_inputs: [[u8; 5]; 10] = [

    //         // rows
    //         [3, 0, 3, 7, 3],
    //         [2, 5, 5, 1, 2],
    //         [6, 5, 3, 3, 2],
    //         [3, 3, 5, 4, 9],
    //         [3, 5, 3, 9, 0],

    //         // collumns
    //         [3, 2, 6, 3, 3],
    //         [0, 5, 5, 3, 5],
    //         [3, 5, 3, 5, 3],
    //         [7, 1, 3, 4, 9],
    //         [3, 2, 2, 9, 0],
    //     ];
    //     let test_results: [u32; 10] = [
    //         3, 
    //         4, 
    //         4, 
    //         3, 
    //         4,

    //         3,
    //         3,
    //         4,
    //         2,
    //         3,            
    //     ];

    //     for (input, result) in iter::zip(test_inputs, test_results) {
    //         let mut tree_line = TreeLine::new();
    //         println!("{:?} {}", input, result);
    //         for i in input {
    //             tree_line.push(i);
    //         }
    //         assert_eq!(
    //             tree_line.visible_trees(), 
    //             result,
    //             "\nInput: {:?}\nTreeLine: {:?}\n Result: {}", 
    //             input,
    //             tree_line,
    //             result
    //         )
    //     }
    // }

    #[test]
    fn example() {
        let result = visible_trees_in_forest("data/example.txt").unwrap();

        assert_eq!(result, 21)
    }

}
