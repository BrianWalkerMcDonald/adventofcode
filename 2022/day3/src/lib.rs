use common::AdventDay;
use std::fs::File;
use std::io;
use std::path::Path;

trait Encoded {
    fn decrypt(&self) -> u32;
}

impl Encoded for char {
    fn decrypt(&self) -> u32 {
        let lower_a = 'a' as u32;
        let upper_a = 'A' as u32;
        let oc = *self as u32;

        if oc <= lower_a {
            oc - upper_a + 27
        } else {
            oc - lower_a + 1
        }
    }
}


struct DataReader<R> {
    data: R,
}

impl DataReader<io::BufReader<File>> {
    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self {
            data: io::BufReader::new(file),
        })
    }
}

impl<R: io::BufRead> Iterator for DataReader<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        let mut buf = String::new();
        let bytes = self.data.read_line(&mut buf).unwrap();

        if bytes == 0 {
            None
        } else {
            let line_len = buf.len();
            let (first_half, second_half) = buf.trim().split_at(line_len / 2);

            let overlapped_char = first_half
                .chars()
                .filter(|c| second_half.contains(*c))
                .next()
                .unwrap();

            Some(overlapped_char.decrypt())
        }
    }
}


struct DataReader2<R> {
    data: R,
}

impl DataReader2<io::BufReader<File>> {
    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self {
            data: io::BufReader::new(file),
        })
    }
}

impl<R: io::BufRead> Iterator for DataReader2<R> {
    type Item = u32;

    fn next(&mut self) -> Option<Self::Item> {
        
        let mut first_line = String::new();
        let bytes = self.data.read_line(&mut first_line).unwrap();
        if bytes == 0 {
            return None;
        }

        let mut second_line = String::new();
        let bytes = self.data.read_line(&mut second_line).unwrap();
        if bytes == 0 {
            panic!("wait a second ... ")
        }

        let mut third_line = String::new();
        let bytes = self.data.read_line(&mut third_line).unwrap();
        if bytes == 0 {
            panic!("well that's not right ... ")
        }

        for c1 in first_line.chars() {
            for c2 in second_line.chars() {
                for c3 in third_line.chars() {
                    if c1 == c2 && c2 == c3 && c3 == c1 {
                        return Some(c1.decrypt())
                    }
                }
            }
        }

        panic!("Whoopsie");
    }
}

pub struct Day3;

impl Day3 {
    #[allow(dead_code)]
    fn example1() -> u32 {
        let data_reader = DataReader::from_file("data/example.txt").unwrap();

        data_reader.sum()
    }

    #[allow(dead_code)]
    fn example2() -> u32 {
        let data_reader = DataReader2::from_file("data/example.txt").unwrap();

        data_reader.sum()
    }
}

impl AdventDay<u32> for Day3 {
    fn part1() -> u32 {
        let data_reader = DataReader::from_file("day3/data/input.txt").unwrap();

        data_reader.sum()
    }

    fn part2() -> u32 {
        let data_reader = DataReader2::from_file("day3/data/input.txt").unwrap();

        data_reader.sum()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1() {
        assert_eq!(Day3::example1(), 157);
    }

    #[test]
    fn example2() {
        assert_eq!(Day3::example2(), 70);
    }
}
