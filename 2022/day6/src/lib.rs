use std::collections::HashSet;
use std::fs::File;
use std::io;
use common::AdventDay;

const START_OF_PACKET: usize = 4;
const START_OF_MESSAGE: usize = 14;

fn index_after_unique_window<R: io::BufRead> (mut input_buffer: R, window_size: usize) -> Result<usize, ()> {
    let mut buf = String::new();
    let bytes = input_buffer.read_line(&mut buf).expect("Couldn't read line");
    if bytes == 0 {
        panic!("why end of file?");
    }

    let vec_char: Vec<char> = buf.chars().collect();

    for (index, window) in vec_char.windows(window_size).enumerate() {
        let mut char_set: HashSet<char> = HashSet::new();
        char_set.extend(window.iter());

        if char_set.len() == window_size {
            return Ok(index + window_size);
        }
    };

    Err(())
}


pub struct Day6;

impl AdventDay<usize> for Day6 {
    fn part1() -> usize {
        let file = File::open("day6/data/input.txt").expect("Couldn't load input file");

        index_after_unique_window(io::BufReader::new(file), START_OF_PACKET).unwrap()
    }

    fn part2() -> usize {
        let file = File::open("day6/data/input.txt").expect("Couldn't load input file");

        index_after_unique_window(io::BufReader::new(file), START_OF_MESSAGE).unwrap()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example1_pt1() {
        let cursor = io::Cursor::new(b"mjqjpqmgbljsphdztnvjfqwrcgsmlb");

        let result = index_after_unique_window(cursor, START_OF_PACKET).unwrap();
        assert_eq!(result, 7);
    }
    
    #[test]
    fn example2_pt1() {
        let cursor = io::Cursor::new(b"bvwbjplbgvbhsrlpgdmjqwftvncz");

        let result = index_after_unique_window(cursor, START_OF_PACKET).unwrap();
        assert_eq!(result, 5);
    }
    
    #[test]
    fn example3_pt1() {
        let cursor = io::Cursor::new(b"nppdvjthqldpwncqszvftbrmjlhg");

        let result = index_after_unique_window(cursor, START_OF_PACKET).unwrap();
        assert_eq!(result, 6);
    }
    
    #[test]
    fn example4_pt1() {
        let cursor = io::Cursor::new(b"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg");


        let result = index_after_unique_window(cursor, START_OF_PACKET).unwrap();
        assert_eq!(result, 10);
    }
    
    #[test]
    fn example5_pt1() {
        let cursor = io::Cursor::new(b"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw");

        let result = index_after_unique_window(cursor, START_OF_PACKET).unwrap();
        assert_eq!(result, 11);
    }

    #[test]
    fn example1_pt2() {
        let cursor = io::Cursor::new(b"mjqjpqmgbljsphdztnvjfqwrcgsmlb");

        let result = index_after_unique_window(cursor, START_OF_MESSAGE).unwrap();
        assert_eq!(result, 19);
    }
    
    #[test]
    fn example2_pt2() {
        let cursor = io::Cursor::new(b"bvwbjplbgvbhsrlpgdmjqwftvncz");

        let result = index_after_unique_window(cursor, START_OF_MESSAGE).unwrap();
        assert_eq!(result, 23);
    }
    
    #[test]
    fn example3_pt2() {
        let cursor = io::Cursor::new(b"nppdvjthqldpwncqszvftbrmjlhg");

        let result = index_after_unique_window(cursor, START_OF_MESSAGE).unwrap();
        assert_eq!(result, 23);
    }
    
    #[test]
    fn example4_pt2() {
        let cursor = io::Cursor::new(b"nznrnfrfntjfmvfwmzdfjlvtqnbhcprsg");


        let result = index_after_unique_window(cursor, START_OF_MESSAGE).unwrap();
        assert_eq!(result, 29);
    }
    
    #[test]
    fn example5_pt2() {
        let cursor = io::Cursor::new(b"zcfzfwzzqfrljwzlrfnpqdbhtmscgvjw");

        let result = index_after_unique_window(cursor, START_OF_MESSAGE).unwrap();
        assert_eq!(result, 26);
    }
}
