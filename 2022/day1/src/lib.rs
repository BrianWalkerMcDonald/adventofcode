use std::collections::VecDeque;
use std::path::Path;
use std::fs::File;
use std::io;
use common::AdventDay;

#[derive(Debug)]
struct Elf {
    total: u32,
}

impl Elf {
    fn from_calorie_list(calorie_list: Vec<u32>) -> Self {
        let total =  calorie_list.iter().sum();
        Elf { total }
    }
}

struct ElfIterator<R> 
where R: io::BufRead{
    lines_iterator: R,
}

impl ElfIterator<io::BufReader<File>> {

    fn from_file(path: &str) -> io::Result<Self> {
        let filepath = Path::new(path);

        let file = File::open(filepath)?;
        Ok(Self { lines_iterator: io::BufReader::new(file) })
    }

}

impl<R: io::BufRead> Iterator for ElfIterator<R> {
    type Item = Elf;

    fn next(&mut self) -> Option<Self::Item> {
        
        let mut calorie_list: Vec<u32> = vec![];

        let mut buf = String::new();
        let mut bytes = self.lines_iterator.read_line(&mut buf).unwrap();
        
        while bytes != 0 && buf != "\n" {
            let snack_calorie = buf.trim().parse::<u32>().unwrap();
            calorie_list.push(snack_calorie);

            buf.clear();
            bytes = self.lines_iterator.read_line(&mut buf).unwrap();
        }

        if bytes != 0 {
            Some(Elf::from_calorie_list(calorie_list))
        } else {
            if !calorie_list.is_empty() {
                Some(Elf::from_calorie_list(calorie_list))
            } else {
                None
            }
        }     
    }
}


pub struct Day1;

impl Day1 {
    fn max_elf_calorie(file: &str) -> u32 {
        let data = ElfIterator::from_file(file).unwrap();

        data.map( |elf| elf.total ).max().unwrap()
    }

    fn sum_three_max_elf_calorie(file: &str) -> u32 {
        let mut maxes = VecDeque::from([0, 0, 0]);
        let data = ElfIterator::from_file(file).unwrap();

        for elf in data {
            if elf.total > maxes[2] {
                maxes.pop_back();

                match elf.total {
                    x if &x < maxes.back().unwrap() => { maxes.push_back(x) },
                    x if &x > maxes.front().unwrap() => { maxes.push_front(x) },
                    x => { maxes.insert(1, x)}
                }
            }
        }

        maxes.iter().sum()
    }

    #[allow(dead_code)]
    fn example_part1() -> u32 {
        Self::max_elf_calorie("data/example.txt")
    }

    #[allow(dead_code)]
    fn example_part2() -> u32 {
        Self::sum_three_max_elf_calorie("data/example.txt")
    }
}


impl AdventDay<u32> for Day1 {
    fn part1() -> u32 {
        Self::max_elf_calorie("day1/data/input.txt")
    }

    fn part2() -> u32 {
        Self::sum_three_max_elf_calorie("day1/data/input.txt")
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn elf_iterator() {
        let cursor = io::Cursor::new(b"10\n5\n\n30");

        let mut elf_iter = ElfIterator { lines_iterator: cursor };

        let elf = elf_iter.next().unwrap();
        assert_eq!(elf.total, 15);

        let elf = elf_iter.next().unwrap();
        assert_eq!(elf.total, 30);

        let elf = elf_iter.next();
        assert!(elf.is_none());
    }

    #[test]
    fn part1_example() {
        assert_eq!(Day1::example_part1(), 24000);
    }

    #[test]
    fn part2_example() {
        assert_eq!(Day1::example_part2(), 45000);
    }
}
