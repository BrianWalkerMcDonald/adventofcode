# 2022

I'm giving a new project layout a shot, using cargo workspaces to divide up the days 
and a 'advent_runner' project to execute puzzle soling code.
Also using 'tests' to run the examples. 
So the following commands should work in this directory

print out all answers
```bash
cargo run
```

run tests for a day
```bash
cargo test -p day1
```