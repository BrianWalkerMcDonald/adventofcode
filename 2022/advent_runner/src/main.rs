use common::AdventDay;
use day1::Day1;
use day2::Day2;
use day3::Day3;
use day4::Day4;
use day5::Day5;
use day6::Day6;
use day7::Day7;
use day8::Day8;

fn main() {
    println!("Hello advent of code!");

    // TODO: how to iterate of structs as namespaces, boxes didn't work

    println!("-- Day 1 --");
    println!("part1 answer: {:?}", Day1::part1());
    println!("part2 answer: {:?}", Day1::part2());

    println!("-- Day2 --");
    println!("part1 answer: {:?}", Day2::part1());
    println!("part2 answer: {:?}", Day2::part2());

    println!("-- Day3 --");
    println!("part1 answer: {:?}", Day3::part1());
    println!("part2 answer: {:?}", Day3::part2());

    println!("-- Day4 --");
    println!("part1 answer: {:?}", Day4::part1());
    println!("part2 answer: {:?}", Day4::part2());

    println!("-- Day5 --");
    println!("part1 answer: {:?}", Day5::part1());
    println!("part2 answer: {:?}", Day5::part2());

    println!("-- Day6 --");
    println!("part1 answer: {:?}", Day6::part1());
    println!("part2 answer: {:?}", Day6::part2());

    println!("-- Day7 --");
    println!("part1 answer: {:?}", Day7::part1());
    println!("part2 answer: {:?}", Day7::part2());

    println!("-- Day8 --");
    println!("part1 answer: {:?}", Day8::part1());
    println!("part2 answer: {:?}", Day8::part2());
}
