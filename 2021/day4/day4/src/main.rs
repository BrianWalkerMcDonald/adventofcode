use std::fs::File;
use std::io::{self, BufRead};


#[derive(Clone,Copy)]
struct BingoBoard {
	values: [u8; 25],
	marked: [bool; 25]
}


impl BingoBoard {

	fn from_vec(data: &Vec<u8>) -> Self {
		let mut values: [u8; 25] = [0; 25];
		let marked: [bool; 25] = [false; 25];

		for (v, d) in values.iter_mut().zip(data){
			*v = *d
		}

		Self { values, marked }
	}


	fn search(&self, value: u8) -> Option<usize> {
		// LEARN: huh, no index type of method?
		self.values.iter().position(| &x | x == value)
	}

	fn process_pick(&mut self, pick: u8) -> Option<usize> {

		let index_result = self.search(pick);


		let index = match index_result {
			Some(index) => { self.marked[index] = true; index },
			None => { return None; },
		};

		// Debug to pick each board that matched
		// println!("{}", self.to_string());


		// check if won
		let row_index = (index / 5) * 5;
		let matched_row = self.marked[row_index  .. row_index + 5].iter().all(|&x| x);
		let col_index = index % 5;
		let matched_col = (0..5).map(| x | x * 5 + col_index).all(| x | self.marked[x]);
		let won = matched_row || matched_col;
	
		// println!("Matched {:>2} at {}, {}", pick, row_index, col_index);

		if won { Some(self.id(pick)) } else { None }
	}

	fn id(&self, pick: u8) -> usize {

		let board_sum: usize = self.marked.iter()
			.zip(self.values)
			.filter( | (m, _) | !*m )
			.map( | (_, v) | v as usize )
			.sum();

		board_sum * ( pick as usize )
	}

	fn to_string(&self) -> String {
		self.marked.iter()
			.zip(self.values)
			.enumerate()
			.map( | (i, (m, v)) | {
				let color = if *m { "\x1b[1;33m" } else { "" };
				let color_end = if *m { "\x1b[0m" } else { "" };
				let nl = if i % 5 == 4 { "\n" } else { " " };
				format!(
					"{}{:>2}{}{}",
					color,
					v,
					color_end,
					nl,
				)
			})
			.collect::<Vec<String>>()
			.join("")
	}

}


struct BingoRunner {
	boards: Vec<BingoBoard>,
	picks: Vec<u8>,
}


impl BingoRunner {
	fn from_file(filename: &str) -> Self {
		let file = File::open(filename).unwrap();	
		let mut buf_read = io::BufReader::new(file);

		// LEARN: couldn't figure out how to iterate over file split on double newlines

		let mut buf = String::new();
		buf_read.read_line(&mut buf).unwrap();

		// assuming \n not \r\n
		buf.pop();
		let picks: Vec<u8> = buf.split(",").map(| x | x.parse::<u8>().unwrap()).collect();

		// blank line
		let mut buf = String::new();
		buf_read.read_line(&mut buf).unwrap();


		// parse boards
		let mut boards: Vec<BingoBoard> = Vec::new();

		let mut buffer: Vec<u8> = Vec::new();

		for line in buf_read.lines() {
			let line = line.unwrap();

			if line.len() == 0 {

				if buffer.len() > 0 {
					boards.push( BingoBoard::from_vec(&buffer) );
				}
				buffer.truncate(0);

			} else {
				for value in line.split_whitespace().map(| x | x.parse::<u8>().unwrap()) {
					if buffer.contains(&value) {
						panic!("Double number occurs on board {}", boards.len() + 1);
					}
					buffer.push(value);
				}

			}
		}

		if buffer.len() > 0 {
			boards.push( BingoBoard::from_vec(&buffer) );
		}


		Self {	boards, picks }
	}

	fn run_first_winners(&mut self) -> Option<Vec<usize>> {

		// lets do a check for multiple winners
		let mut winners: Vec<usize> = Vec::new();

		for pick in self.picks.iter() {
			for (board_num, board) in self.boards.iter_mut().enumerate() {
				let board_result = board.process_pick(*pick);

				if let Some(board_id) = board_result {
					println!();
					println!("Winner board {}", board_num);
					println!();
					println!("{}", board.to_string());
					
					winners.push(board_id);
				}
			}

			if winners.len() > 0 {
				break;
			}
		}

		if winners.len() > 0 {
			Some(winners)
		} else {
			None
		}
	}


	fn run_last_winner(&mut self) -> Option<usize> {
		// This assumes it doesn't end in a tie

		// lets do a check for multiple winners
		let mut ignore_winners: Vec<usize> = Vec::new();
		let number_wins_to_ignore: usize = self.boards.len() - 1;



		for pick in self.picks.iter() {
			for (board_num, board) in self.boards.iter_mut().enumerate() {

				if ignore_winners.contains(&board_num) {
					continue;
				}

				let board_result = board.process_pick(*pick);

				if let Some(board_id) = board_result {
					if ignore_winners.len() < number_wins_to_ignore {
						ignore_winners.push(board_num);	
					} else {
						return Some(board_id);
					}					
				}
			}
		}

		None
	}
}


fn main() {
	let mut bingo_runner = BingoRunner::from_file("../input");
	let winners = bingo_runner.run_first_winners();

	match winners {
		None => panic!("No winners"),
		Some(win) if win.len() > 1 => panic!("Multiple winners"),
		Some(win) => println!("The winner is {}",  win[0]),
	}

	let last_winner = bingo_runner.run_last_winner();

	match last_winner {
		None => panic!("No last winner"),
		Some(id) => println!("The last winner is {:?}", id),
	}
}
