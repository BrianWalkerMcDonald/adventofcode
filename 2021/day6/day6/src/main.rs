use std::fs::File;
use std::io::{self, BufRead};
use std::collections::VecDeque;


#[derive(Debug)]
struct LanternfishTracker {
	population_tracker: VecDeque<usize>,
}

impl LanternfishTracker {
	fn from_file(filename: &str) -> Self {
		let max_lifetime = 9;
		let mut population_tracker: VecDeque<usize> = VecDeque::with_capacity(max_lifetime);
		// LEARN: not sure how to init
		for _ in 0..max_lifetime {
			population_tracker.push_back(0)
		}


		let file = File::open(filename).unwrap();	
		let mut buf_read = io::BufReader::new(file);

		let mut first_line = String::new();
		buf_read.read_line(&mut first_line).unwrap();

		// remove \n
		first_line.pop();
		let lifetime_gen = first_line.split(",").map(| x | x.parse::<usize>().unwrap());

		for lifetime in lifetime_gen {
			let population = population_tracker.get_mut(lifetime).unwrap();
			*population += 1;
		}

		Self { population_tracker }
	}

	fn step_day(&mut self) {
		// back is 
		let new_laternfish = self.population_tracker.pop_front().unwrap();
		self.population_tracker.push_back(new_laternfish);

		let seventh_day = self.population_tracker.get_mut(6).unwrap();
		*seventh_day += new_laternfish;
	}

	fn step_n_days(&mut self, n: usize) {
		for _ in 0..n { self.step_day(); }
	}

	fn count(&self) -> usize {
		self.population_tracker.iter().sum()
	}
}




fn main() {
    let mut laternfish_tracker = LanternfishTracker::from_file("../input");

    laternfish_tracker.step_n_days(256);
    println!("Number of fish: {}", laternfish_tracker.count());
}
