use std::fs::File;
use std::io::{self, BufRead};


enum Movement {
	FORWARD(i64),
	DOWN(i64),
	UP(i64)
}


#[derive(Default)]
struct Sub {
	horizontal: i64,
	depth: i64,
	aim: i64,
}

impl Sub {
	fn process_movement_part1 (&mut self, movement: Movement ) {
		match movement {
			Movement::FORWARD(x) => self.horizontal += x,
			Movement::DOWN(x) => self.depth += x,
			Movement::UP(x) => self.depth -= x
		};
	}

	fn process_movement_part2 (&mut self, movement: Movement ) {
		match movement {
			Movement::FORWARD(x) => {
				self.horizontal += x;
				self.depth += self.aim * x;
			}
			Movement::DOWN(x) => self.aim += x,
			Movement::UP(x) => self.aim -= x
		};

	}

	fn multi_depth_horz(self) -> i64 {
		self.horizontal * self.depth
	}
}


struct ReadInputFile {
	lines: io::Lines<io::BufReader<File>>
}

impl ReadInputFile {

	fn from_filename(filename: &str) -> Self {
		let file = File::open(filename).unwrap();		

		ReadInputFile {
			lines: io::BufReader::new(file).lines()
		}
	}
}

impl Iterator for ReadInputFile {
	type Item = Movement;

	fn next(&mut self) -> Option<Self::Item> {
		let line = match self.lines.next() {
			Some(Ok(line)) => line,
			Some(Err(e)) => panic!("{:?}", e),
			None => return None,
		};

		let split_line = line.split_whitespace().collect::<Vec<&str>>();

		match split_line[..] {
			["forward", x] => Some(Movement::FORWARD(x.parse::<i64>().unwrap())),
			["down", x] => Some(Movement::DOWN(x.parse::<i64>().unwrap())),
			["up", x] => Some(Movement::UP(x.parse::<i64>().unwrap())),
			_ => panic!("unknown line")
		}
	}
}

fn part1() -> i64 {
    let read_input = ReadInputFile::from_filename("../input");
    let mut sub = Sub::default();

    for movement in read_input {
    	sub.process_movement_part1(movement);
    }

    sub.multi_depth_horz()
}


fn part2() -> i64 {
    let read_input = ReadInputFile::from_filename("../input");
    let mut sub = Sub::default();

    for movement in read_input {
    	sub.process_movement_part2(movement);
    }

    sub.multi_depth_horz()
}


fn main() {

	println!("Part 1: Answer {}", part1());
	println!("Part 2: Answer {}", part2());

}
