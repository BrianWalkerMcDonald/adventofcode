use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashSet;
use std::cmp::Ordering;

type Point = (isize, isize);


struct Vec2D<T>
{
	data: Vec<T>,
	max_row: isize,
	max_col: isize,
}

impl<T> Vec2D<T> 
where 
	T: Copy
{
	fn new() -> Self {
		Self {
			data: Vec::new(),
			max_row: 0,
			max_col: 0,
		}
	}

	fn push_row(&mut self, row: Vec<T>) -> Result<(), String>{
		self.max_row += 1;
		if self.max_col == 0 {
			self.max_col = row.len() as isize;
		} else if self.max_col != row.len() as isize {
			return Err("Rows of different length".to_string())
		}

		for r in row {
		 	self.data.push(r);
		 }

		Ok(())
	}

	fn get<'a>(&'a self, point: Point) -> Option<&'a T> {
		if let Some(index) = self.point_to_index(point) {
			self.data.get(index)	
		} else {
			None
		}
	}

	fn point_to_index(&self, point: Point) -> Option<usize> {
		if point.0 < 0 || point.0 >= self.max_row ||
		   point.1 < 0 || point.1 >= self.max_col {
			None 
		} else {
			Some((point.0 * self.max_col + point.1) as usize)
		}
	}

	fn iter_points(&self) -> IterPoints {
		IterPoints::new(self.max_row, self.max_col)
	}

	fn neighors(&self, point: Point) -> Vec<Point> {
		let mut answer = Vec::new();

		if point.0 != 0 {
			answer.push((point.0 - 1, point.1))
		} 
		if point.0 < self.max_row - 1 {
			answer.push((point.0 + 1, point.1))	
		}  
		if point.1 != 0 {
			answer.push((point.0, point.1 - 1))
		} 
		if point.1 < self.max_col - 1 {
			answer.push((point.0, point.1 + 1))	
		}  

		answer
	}
}

struct IterPoints {
	row: isize,
	col: isize,
	max_row: isize,
	max_col: isize,
}

impl IterPoints {
	fn new(max_row: isize, max_col: isize) -> Self {
		Self {
			row: 0,
			col: -1,
			max_row,
			max_col
		}
	}
}

impl Iterator for IterPoints {
	type Item = Point;

	fn next(&mut self) -> Option<Self::Item> {
		self.col += 1;

		if self.col >= self.max_col {
			self.col = 0;
			self.row += 1;
		}

		if self.row >= self.max_row {
			return None;
		}

		Some((self.row, self.col))
	}
}



struct HeightMap {
	heights: Vec2D<u32>
}


impl  HeightMap {
	fn from_file(filename: &str) -> Self {
	    let file = File::open(filename).unwrap();   
	    let buf_read = io::BufReader::new(file);

	    let mut heights = Vec2D::new();

	    for line in buf_read.lines() {
	    	let line = line.unwrap();

	    	let row: Vec<u32> = line.chars().map( | h | h.to_digit(10).unwrap() ).collect();

	    	heights.push_row(row).unwrap();
	    }

	    Self {
	    	heights,
	    }
	}

	fn low_points(&self) -> Vec<Point> {
		let mut answer = Vec::new();

		for point in self.heights.iter_points() {
			let point_height = *self.heights.get(point).unwrap();

	    	if self.heights.neighors(point).iter()
	    		.filter_map( | pt | self.heights.get(*pt))
	    		.all( | nieghbor_height | *nieghbor_height > point_height )
	    	{
	    		answer.push(point);
	    	}
	    }

	    answer
	}

	fn find_basin_sizes(self) -> Vec<usize> {

		let low_points = self.low_points();
		let mut answer = Vec::new();

		for point in low_points {
			let mut basin_points = HashSet::new();
			let mut explore_points = Vec::new();
			let mut searched_points = HashSet::new();

			explore_points.push(point);
			while explore_points.len() != 0 {

				// LEARN: HashSet pop
				let popped_point = explore_points.pop().unwrap();
				searched_points.insert(popped_point);

				if let Some(height) = self.heights.get(popped_point) {
					if *height < 9 {
						basin_points.insert(popped_point);

						for neighor_point in self.heights.neighors(popped_point) {
							if searched_points.insert(neighor_point) {
								explore_points.push(neighor_point);	
							}
						}
					} 
				}
			}

			answer.push(basin_points.len());
		}


		answer
	}
}



fn main() {
    let height_map = HeightMap::from_file("../input");

    let mut basin_sizes = height_map.find_basin_sizes();

    let mut answer: usize = 1;
    for _ in 0..3 {
    	let (index, max) = basin_sizes
	        .iter()
	        .enumerate()
	        .max_by(|(_, a), (_, b)| a.partial_cmp(b).unwrap_or(Ordering::Equal))
	        .map( | (i, m) | {
	        	(i, *m)
        	}).unwrap();

    	basin_sizes.remove(index);
    	answer *= max;
    }

    println!("{:?}", answer);
}
