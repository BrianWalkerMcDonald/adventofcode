use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashSet;

type Point = (isize, isize);


struct Vec2D<T>
{
	data: Vec<T>,
	max_row: isize,
	max_col: isize,
}

impl<T> Vec2D<T> 
where 
	T: Copy
{
	fn new() -> Self {
		Self {
			data: Vec::new(),
			max_row: 0,
			max_col: 0,
		}
	}

	fn push_row(&mut self, row: Vec<T>) -> Result<(), String>{
		self.max_row += 1;
		if self.max_col == 0 {
			self.max_col = row.len() as isize;
		} else if self.max_col != row.len() as isize {
			return Err("Rows of different length".to_string())
		}

		for r in row {
		 	self.data.push(r);
		 }

		Ok(())
	}

	fn get<'a>(&'a self, point: Point) -> Option<&'a T> {
		if let Some(index) = self.point_to_index(point) {
			self.data.get(index)	
		} else {
			None
		}
	}

	fn get_mut<'a>(&'a mut self, point: Point) -> Option<&'a mut T> {
		if let Some(index) = self.point_to_index(point) {
			self.data.get_mut(index)	
		} else {
			None
		}
	}


	fn point_to_index(&self, point: Point) -> Option<usize> {
		if point.0 < 0 || point.0 >= self.max_row ||
		   point.1 < 0 || point.1 >= self.max_col {
			None 
		} else {
			Some((point.0 * self.max_col + point.1) as usize)
		}
	}

	fn iter_points(&self) -> IterPoints {
		IterPoints::new(self.max_row, self.max_col)
	}

	fn neighors(&self, point: Point) -> Vec<Point> {
		let mut answer = Vec::new();

		let (r, c) = point;

		let mut cols = vec![0];
		let mut rows = vec![0];

		if r > 0 {
			rows.push(-1);
		}
		if r < self.max_row - 1 {
			rows.push(1);	
		}
		if c > 0 {
			cols.push(-1);
		}
		if c < self.max_col -1 {
			cols.push(1);
		}

		for offset_r in rows.iter() {
			for offset_c in cols.iter() {
				if (0, 0) == (*offset_r, *offset_c) {
					continue;
				}
				answer.push((r + *offset_r, c + *offset_c));
			}
		}  

		answer
	}
}

struct IterPoints {
	row: isize,
	col: isize,
	max_row: isize,
	max_col: isize,
}

impl IterPoints {
	fn new(max_row: isize, max_col: isize) -> Self {
		Self {
			row: 0,
			col: -1,
			max_row,
			max_col
		}
	}
}

impl Iterator for IterPoints {
	type Item = Point;

	fn next(&mut self) -> Option<Self::Item> {
		self.col += 1;

		if self.col >= self.max_col {
			self.col = 0;
			self.row += 1;
		}

		if self.row >= self.max_row {
			return None;
		}

		Some((self.row, self.col))
	}
}


struct OctStepper {
	energies: Vec2D<u32>
}


impl OctStepper {
	fn from_file(filename: &str) -> Self {
	    let file = File::open(filename).unwrap();   
	    let buf_read = io::BufReader::new(file);

	    let mut energies = Vec2D::new();

	    for line in buf_read.lines() {
	    	let line = line.unwrap();

	    	let row: Vec<u32> = line.chars().map( | h | h.to_digit(10).unwrap() ).collect();

	    	energies.push_row(row).unwrap();
	    }

	    Self {
	    	energies,
	    }
	}

	fn step(&mut self) -> usize {
		let mut flashed = HashSet::new();

		// first pass on all, collecting flash points
		for point in self.energies.iter_points() {
			let eng = self.energies.get_mut(point).unwrap();
			*eng += 1;

			if *eng > 9 {
				flashed.insert(point);
			}
		}

		// iterate on nieghbors of flashed points until there are none left
		let mut flash_add: Vec<Point> = flashed.iter().flat_map(| pt | self.energies.neighors(*pt)).collect();

		while flash_add.len() > 0 {
			let mut next_loop = Vec::new();

			for point in flash_add {
				let eng = self.energies.get_mut(point).unwrap();
				*eng += 1;

				if *eng > 9 {
					if flashed.insert(point) {
						next_loop.push(point);
					};
				}
			}

			flash_add = next_loop.iter().flat_map(| pt | self.energies.neighors(*pt)).collect();
		}

		for point in flashed.iter() {
			let eng = self.energies.get_mut(*point).unwrap();
			*eng = 0;
		}

		flashed.len()
	}

	#[allow(dead_code)]
	fn to_string(&self) -> String {

		let mut output = String::new();

		let mut prev_row = 0;
		for point in self.energies.iter_points() {
			if point.0 != prev_row {
				prev_row = point.0;
				output += "\n";
			}

			let value = self.energies.get(point).unwrap();
			let (color, color_end) = match *value { 
				0 => ("\x1b[1;38;5;9m", "\x1b[0m"),
				9 => ("\x1b[1;32m", "\x1b[0m"),
				_ => ("\x1b[1;38;5;240m", "\x1b[0m"),
			};
				
			output += &format!(
				"{}{}{}",
				color,
				value,
				color_end,
			);
		}

		output
	}

}


fn main() {
	let mut oct_stepper = OctStepper::from_file("../input");

	let mut step_count: usize = 1;
	while oct_stepper.step() != 100 {
		step_count += 1;
	}

	println!("First all falsh: {}", step_count);
}