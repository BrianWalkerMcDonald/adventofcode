use std::fs::File;
use std::io::{self, BufRead};


fn part1() -> Result<u32, io::Error> {
	let file = File::open("../input")?;
	let lines = io::BufReader::new(file).lines();

	// assume prev_value is bigger then first value to ignore
	let mut prev_value = 10000;

	let mut answer = 0;

	for line in lines {
		let string_line = line?;

		let value = string_line.parse::<u32>().unwrap();

		if value > prev_value {
			answer += 1;
		}

		prev_value = value;
	}

	Ok(answer)
}


fn part2() -> u32 {
	let file = File::open("../input").unwrap();
	let lines = io::BufReader::new(file).lines();


	// Couldn't get this to work without the collect
	lines
		.map( | x | x.unwrap().parse::<u32>().unwrap() )
		.collect::<Vec<u32>>()
		.windows(3)
		.map( | x | x.iter().sum::<u32>() )
		.collect::<Vec<u32>>()
		.windows(2)
		.fold(0, |acc, x| acc + (x[1] > x[0]) as u32)
}


fn main() {
    
    println!("Part 1: Answer {}", part1().unwrap());

    println!("Part 2: Answer {}", part2());

}
