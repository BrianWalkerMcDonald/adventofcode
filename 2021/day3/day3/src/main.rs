use std::fs::File;
use std::io::{self, BufRead};


#[derive(Debug)]
struct DiagnosticOutput {
	epsilon: usize,
	gamma: usize,
	power: usize,
	oxygen: usize,
	co2: usize,
	life_support: usize,
}


#[derive(Debug)]
struct BitTracker<const COUNT: usize> {
	counter: [i64; COUNT],
	max: usize,
}

impl<const COUNT: usize> BitTracker<COUNT> {
	fn default() -> Self {
		Self {
			counter: [0; COUNT],
			max: 1 << COUNT
		}
	}

	fn process(&mut self, value: usize) {
		if value > self.max {
			panic!("{} bigger than max {}", value, self.max);
		}

		for (i, v) in self.counter.iter_mut().enumerate() {
			let mask = 1 << (COUNT - i - 1);
			if (value & mask) != 0 {
				*v += 1
			} else { 
				*v -= 1 
			}
		}
	}

	
	fn most_and_least(&self) -> (usize, usize)  {
		let most: usize = self.counter
			.iter()
			.map(| x | {
				if x > &0 { 1 } else { 0	}
			})
			.fold(0, | acc, x | (acc << 1) + x );

		// LEARN: stupid u16 here
		let mask = 2_u16.pow(COUNT as u32) - 1;

		let least: usize = !most & ( mask as usize );

		(most as usize, least as usize)
	}
}


fn b_string_to_usize(input_string: String) -> usize {
	input_string.chars()
		.map( | x | 
			match x {
				'0' => 0,
				'1' => 1,
				x => panic!("unknown char {:?}", x),
			}
		)
		.fold(0, | acc, x | (acc << 1) + x )
}


struct DiagnosticLog<const COUNT: usize> {
	tracker: BitTracker<COUNT>,
	values: Vec<usize>,
}

impl<const COUNT: usize> DiagnosticLog<COUNT> {

	fn from_filename(filename: &str) -> Self {
		let file = File::open(filename).unwrap();		

		let mut result = Self {
			tracker: BitTracker::<COUNT>::default(),
			values: Vec::new(),
		};

		for line in io::BufReader::new(file).lines() {
			let value = b_string_to_usize(line.unwrap());
			result.tracker.process(value);
			result.values.push(value)
		}

		result
	}

	// LEARN: I don't like how many Vecs I am making, is there a smarter way?
	fn life_support_recurse(mut input_vec: Vec<usize>, most_common: bool) -> usize {
		for i in (0..(COUNT-1)).rev() {
			let mask = 1 << i;
			let mut running_sum: f32 = 0.0;
			let mut over_vec = Vec::new();
			let mut under_vec = Vec::new();
			let len_half: f32 = input_vec.len() as f32 / 2.0;

			for val in input_vec {
				if (val & mask) != 0 {
					running_sum += 1.0;
					over_vec.push(val);
				} else {
					under_vec.push(val);
				}
			}

			if (running_sum >= len_half) == most_common{
				input_vec = over_vec;
			} else {
				input_vec = under_vec;
			}

			if input_vec.len() == 1 {
				break;
			}

			if input_vec.len() == 0 {
				panic!("Failed to find");
			}
		}
		input_vec[0]
	}


	fn diagnostic(self) -> DiagnosticOutput {
		let (gamma, epsilon) = self.tracker.most_and_least();

		let mut one_vec = Vec::new();
		let mut zero_vec = Vec::new();

		let inital_mask = 1 << COUNT - 1;
		for val in self.values.into_iter() {
			if (val & inital_mask) != 0 {
				one_vec.push(val);
			} else {
				zero_vec.push(val);
			}
		}

		let oxygen_vec: Vec<usize>;
		let co2_vec: Vec<usize>;

		if self.tracker.counter[0] > 0 {
			oxygen_vec = one_vec;
			co2_vec = zero_vec;
		} else {
			oxygen_vec = zero_vec;
			co2_vec = one_vec;
		}

		let oxygen = Self::life_support_recurse(oxygen_vec, true);
		let co2 = Self::life_support_recurse(co2_vec, false);


		DiagnosticOutput {
			gamma,
			epsilon,
			power: gamma * epsilon,
			oxygen,
			co2,
			life_support: oxygen * co2,
		}
	}
}


fn main() {
	// let diagnostic_log = DiagnosticLog::<5>::from_filename("../example");
	let diagnostic_log = DiagnosticLog::<12>::from_filename("../input");

	println!("{:?}", diagnostic_log.diagnostic());
}
