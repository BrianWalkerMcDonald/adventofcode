use day13::PaperFolder;

pub fn main() {
    let mut paper_folder = PaperFolder::from_file("../input").expect("Could not load file");

    paper_folder.process_fold();


    println!("Number of dots {}", paper_folder.number_of_dots());
}