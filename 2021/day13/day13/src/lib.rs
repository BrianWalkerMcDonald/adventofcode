use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashSet;


#[derive(Debug)]
pub enum Error {
	FileLoad(String),
}


#[derive(Debug)]
enum Fold {
	X(isize),
	Y(isize),
}


#[derive(Eq, PartialEq, Hash, Debug)]
struct Dot {
	x: isize,
	y: isize,
}


impl Dot {
	fn fold(&self, fold: &Fold) -> Self {
		match fold {
			Fold::X(val) => {
				let x = if self.x > *val { *val - (*val - self.x).abs() } else { self.x };
				Self { x, y: self.y }
			}
			Fold::Y(val) => {
				let y = if self.y > *val { *val - (*val - self.y).abs() } else { self.y };
				Self { x: self.x, y }
			}
		}
	}
}


#[derive(Debug)]
pub struct PaperFolder {
	dots: HashSet<Dot>,
	folds: Vec<Fold>,
}

impl PaperFolder {

	pub fn from_file(filename: &str) -> Result<Self, Error> {
		let file = match File::open(filename) {
			Ok(file) => file,
			Err(_) => return Err(Error::FileLoad("Unable to open file".to_string())),
		};

		let buffer = BufReader::new(file);

		// Parse Dots
		let mut parse_dots = true;
		let mut dots = HashSet::new();
		let mut folds = Vec::new();

		for line in buffer.lines() {

			let line = match line {
				Ok(line) => line,
				Err(_) => return Err(Error::FileLoad("Unable to read line".to_string())),
			};

			if parse_dots {

				if line.len() == 0 {
					parse_dots = false;
					continue;
				}

				let split_line: Vec<&str> = line.split(",").collect();

				if split_line.len() != 2 {
					return Err(Error::FileLoad("Line does not have a single delimiter".to_string()));
				}

				let x = match split_line.first().unwrap().parse::<isize>() {
					Ok(v) => v,
					Err(_) => return Err(Error::FileLoad("X is not a isize".to_string())),
				};
				let y = match split_line.last().unwrap().parse::<isize>() {
					Ok(v) => v,
					Err(_) => return Err(Error::FileLoad("Y is not a isize".to_string())),
				};

				dots.insert(Dot { x, y });

			} else {

				let fold_str = match line.rsplit(" ").next() {
					Some(fold_str) => fold_str,
					None => return Err(Error::FileLoad("Couldn't find x=2".to_string())),
				};

				let split_fold: Vec<&str> = fold_str.split("=").collect();

				if split_fold.len() != 2 {
					return Err(Error::FileLoad("Line does not have a single delimiter".to_string()));
				}

				let fold_type = split_fold.first().unwrap();
				let fold_value = match split_fold.last().unwrap().parse::<isize>() {
					Ok(v) => v,
					Err(_) => return Err(Error::FileLoad("Fold is not a isize".to_string())),
				};

				let fold = match fold_type {
					&"x" => Fold::X(fold_value),
					&"y" => Fold::Y(fold_value),
					_ => return Err(Error::FileLoad("Not x or y fold".to_string())),
				};

				folds.push(fold);
			}
		}

		Ok(Self {
			dots,
			folds,
		})
	}

	pub fn process_fold(&mut self) {
		let fold = self.folds.first().unwrap();

		let mut new_dots = HashSet::new();

		for dot in self.dots.iter() {
			new_dots.insert(dot.fold(&fold));
		}

		self.dots = new_dots;
	}

	pub fn process_folds(&mut self)  {
		for fold in &self.folds {
			let mut new_dots = HashSet::new();

			for dot in self.dots.iter() {
				new_dots.insert(dot.fold(&fold));
			}

			self.dots = new_dots;
		}
	}

	pub fn number_of_dots(&self) -> usize {
		self.dots.len()
	}

	pub fn to_string(&self) -> String {
		let mut output = String::new();

		let mut max_x = 0;
		let mut max_y = 0;

		for dot in self.dots.iter() {
			if dot.x > max_x {
				max_x = dot.x;
			}
			if dot.y > max_y {
				max_y = dot.y;
			}
		}

		for y in 0..max_y+1 {
			for x in 0..max_x+1 {
				if self.dots.contains(&Dot{ x, y }) {
					output.push('#');
				} else {
					output.push('.');
				}
			}
			output.push('\n');
		}

		output
	}
}


#[cfg(test)]
mod tests {
	use super::*;

    #[test]
    fn test_part1_example() {
        let mut paper_folder = PaperFolder::from_file("../example").expect("Could not load file");

        println!("\n{}\n", paper_folder.to_string());
        paper_folder.process_fold();
        println!("\n{}\n", paper_folder.to_string());

        assert_eq!(paper_folder.number_of_dots(), 17);
    }
}
