use day14::PolymerBuilder;


pub fn main() {
	let mut polymer_builder = PolymerBuilder::from_file("../input").unwrap();

	for _ in 0..10 {
		polymer_builder.step();
	}


	println!("The answer is {}", polymer_builder.get_most_minus_least());
}