use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::HashMap;

#[derive(Debug)]
pub enum Error {
	FileLoad(String),
}


type CharacterPair = (char, char);

#[derive(Debug)]
pub struct PolymerBuilder {
	pairs: Vec<CharacterPair>,
	polymer_mapper: HashMap<CharacterPair, [CharacterPair; 2]>,
}

impl PolymerBuilder {
	pub fn from_file(filename: &str) -> Result<Self, Error> {
		let file = match File::open(filename) {
			Ok(file) => file,
			Err(_) => return Err(Error::FileLoad("Unable to open file".to_string())),
		};

		let mut buffer = BufReader::new(file);

		let mut template = String::new();
		buffer.read_line(&mut template).unwrap();
		// clear \n
		template.pop();

		let mut pairs = Vec::new();

		for char_pair in template.chars().collect::<Vec<char>>().windows(2) {
			pairs.push((char_pair[0], char_pair[1]))
		}

		let mut _blank = String::new();
		buffer.read_line(&mut _blank).unwrap();

		let mut polymer_mapper = HashMap::new();

		for line in buffer.lines() {
			let line = match line {
				Ok(line) => line,
				Err(_) => return Err(Error::FileLoad("Unable to read line".to_string())),
			};
		
			let pair_insertion: Vec<&str> = line.split(" -> ").collect();
			let pair_key = match pair_insertion.first() {
				Some(pk) => pk,
				None => return Err(Error::FileLoad("Unable to load pair key".to_string())),
			};
			let insert_char = match pair_insertion.last() {
				Some(pk) => pk.chars().next().unwrap(),
				None => return Err(Error::FileLoad("Unable to load insert character".to_string())),
			};

			let char_split: Vec<char> = pair_key.chars().collect();
			if char_split.len() != 2 {
				return Err(Error::FileLoad("Unable to split char_key".to_string()));
			}
			let first_char = *char_split.get(0).unwrap();
			let second_char = *char_split.get(1).unwrap();

			polymer_mapper.insert(
				(first_char, second_char),
				[(first_char, insert_char), (insert_char, second_char)]
			);
		}

		Ok(Self {
			pairs,
			polymer_mapper,
		})
	}

	pub fn step(&mut self) {
		let mut new_pairs = Vec::new();

		for pair in self.pairs.iter() {
			let other_pairs = self.polymer_mapper.get(pair).unwrap();
			new_pairs.extend(other_pairs.iter().cloned());
		}

		self.pairs = new_pairs;
	}

	pub fn count_letters(&self) -> HashMap<char, usize> {
		let mut counters: HashMap<char, usize> = HashMap::new();

		for pair in self.pairs.iter() {
			let value = counters.entry(pair.0).or_insert(0);
			*value += 1;
		}

		let value = counters.entry(self.pairs.last().unwrap().1).or_insert(0);
		*value += 1;

		counters
	}

	pub fn len(&self) -> usize {
		self.pairs.len() + 1
	}

	pub fn to_string(&self) -> String {
		let mut output = String::new();

		for pair in self.pairs.iter() {
			output.push(pair.0);
		}

		output.push(self.pairs.last().unwrap().1);

		output
	}

	pub fn get_most_minus_least(&self) -> usize {

		let counters = self.count_letters();

		let mut min: usize = 10000000000;
		let mut max: usize = 0;

		for (_, v) in counters.iter() {
			if *v > max { max = *v; }
			if *v < min { min = *v; }
		}

		max - min
	}
}



#[cfg(test)]
mod tests {
use super::*;

    #[test]
    fn test_part1_example_step1() {
    	let mut polymer_builder = PolymerBuilder::from_file("../example").unwrap();

    	polymer_builder.step();

    	assert_eq!(7, polymer_builder.len());
    	assert_eq!("NCNBCHB", polymer_builder.to_string());
    }

    #[test]
    fn test_part1_example_step5() {
    	let mut polymer_builder = PolymerBuilder::from_file("../example").unwrap();

    	for _ in 0..5 {
    		polymer_builder.step();
    	}

    	assert_eq!(97, polymer_builder.len());
    }

    #[test]
    fn test_part1_example_step10() {
    	let mut polymer_builder = PolymerBuilder::from_file("../example").unwrap();

    	for _ in 0..10 {
    		polymer_builder.step();
    	}


    	assert_eq!(1588, polymer_builder.get_most_minus_least());
    }
}
