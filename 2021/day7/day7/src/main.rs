use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashMap;


struct CrabCalulator {
	crab_map: HashMap<isize, isize>,
	min_value: isize,
	max_value: isize,
	costs: Vec<isize>,
}

impl CrabCalulator {
	fn from_file(filename: &str) -> Self {
		let file = File::open(filename).unwrap();	
		let mut buf_read = io::BufReader::new(file);

		let mut buf = String::new();
		buf_read.read_line(&mut buf).unwrap();

		buf.pop();
		let crab_pos: Vec<isize> = buf.split(",").map(| x | x.parse::<isize>().unwrap()).collect();

		let mut min_value = 100000000;
		let mut max_value = 0;
	    let mut crab_map: HashMap<isize, isize> = HashMap::new();

	    for value in crab_pos {
	    	if value <  min_value {
	    		min_value = value;
	    	}
	    	if value > max_value {
	    		max_value = value;
	    	}

	    	match crab_map.get_mut(&value) {
	    		Some(x) => {
	    			*x += 1
	    		},
	    		None => {
	    			crab_map.insert(value, 1);
	    		},
	    	}
	    }


	    let mut costs = Vec::new();

	    let mut ans: isize = 0;
	    for v in 0 .. max_value + 1{
	    	ans += v;
	    	costs.push(ans);
	    }

		Self { crab_map, min_value, max_value, costs }
	}


	#[allow(dead_code)]
	fn evaluate_move_linear(&self, eval_pos: isize) -> isize {
		let mut sum = 0;
		for (key, val) in self.crab_map.iter() {	    	
	    	let cost = (key - eval_pos).abs() * val;
	    	sum += cost;
	    }

	    sum
	}

	fn evaluate_move_square(&self, eval_pos: isize) -> isize {

		let mut sum = 0;
		for (key, val) in self.crab_map.iter() {
			let diff = (key - eval_pos).abs();
			let individual_cost: isize =  self.costs[diff as usize];

	    	sum += individual_cost * val;
	    }

	    sum
	}
}


fn main() {
    let crab_calc = CrabCalulator::from_file("../input");


    let mut min_cost: isize = 10000000000;

    for eval_pos in crab_calc.min_value..crab_calc.max_value+1 {
    	let eval = crab_calc.evaluate_move_square(eval_pos);

    	if eval < min_cost {
    		min_cost = eval;
    	}
    }

	println!("lowest cost {}", min_cost);
}
