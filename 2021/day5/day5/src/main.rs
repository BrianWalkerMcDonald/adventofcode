use std::fs::File;
use std::io::{self, BufRead};
use std::cmp::{self, Ordering};
use std::collections::{HashSet, HashMap};


#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
struct Point {
	x: usize,
	y: usize
}


#[derive(PartialEq, Copy, Clone, Debug)]
enum Orientation {
	HORIZONTAL,
	VERTICAL,
	POINT,
	DIAGONAL_Y_UP,
	DIAGONAL_Y_DOWN,
}


#[derive(PartialEq, Debug, Clone, Copy)]
struct Line {
	start: Point,
	end: Point,
	orientation: Orientation,
}
impl Line {
	fn from_points(point_1: Point, point_2: Point) -> Option<Self>{
		match (point_1.x.cmp(&point_2.x), point_1.y.cmp(&point_2.y)) {
			(Ordering::Equal, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::POINT,
					start: point_1,
					end: point_1,
				})
			},
			(Ordering::Equal, Ordering::Less) => {
				Some(Self{
					orientation: Orientation::VERTICAL,
					start: point_1,
					end: point_2,
				})
			},
			(Ordering::Equal, Ordering::Greater) => {
				Some(Self{
					orientation: Orientation::VERTICAL,
					start: point_2,
					end: point_1,
				})
			},
			(Ordering::Less, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::HORIZONTAL,
					start: point_1,
					end: point_2,
				})
			},
			(Ordering::Greater, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::HORIZONTAL,
					start: point_2,
					end: point_1,
				})
			},
			(Ordering::Less, Ordering::Less) => {
				if point_2.x.saturating_sub(point_1.x) == point_2.y.saturating_sub(point_1.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_UP,
						start: point_1,
						end: point_2,
					})		
				} else {
					None
				}
			},
			(Ordering::Greater, Ordering::Greater) => {
				if point_1.x.saturating_sub(point_2.x) == point_1.y.saturating_sub(point_2.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_UP,
						start: point_2,
						end: point_1,
					})		
				} else {
					None
				}
			},
			(Ordering::Less, Ordering::Greater) => {
				if point_2.x.saturating_sub(point_1.x) == point_1.y.saturating_sub(point_2.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_DOWN,
						start: point_1,
						end: point_2,
					})		
				} else {
					None
				}
			},
			(Ordering::Greater, Ordering::Less) => {
				if point_1.x.saturating_sub(point_2.x) == point_2.y.saturating_sub(point_1.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_DOWN,
						start: point_2,
						end: point_1,
					})		
				} else {
					None
				}
			},
		}
	}

	fn point_in_line(&self, pt: Point) -> bool {
		match self.orientation {
			Orientation::HORIZONTAL => pt.y == self.start.y && pt.x >= self.start.x && pt.x <= self.end.x,
			Orientation::VERTICAL => pt.x == self.start.x && pt.y >= self.start.y && pt.y <= self.end.y,
			Orientation::POINT => pt == self.start,
			Orientation::DIAGONAL_Y_DOWN => {
				(pt.x + pt.y) == (self.start.x + self.start.y) && // on the same diagonal
				pt.x >= self.start.x && pt.x <= self.end.x &&
				pt.y >= self.end.y && pt.y <= self.start.y
			},
			Orientation::DIAGONAL_Y_UP => {
				(pt.x as isize - pt.y as isize) == (self.start.x as isize - self.start.y as isize) &&
				pt.x >= self.start.x && pt.x <= self.end.x &&
				pt.y >= self.start.y && pt.y <= self.end.y
			},
		}
	}

	fn overlap(&self, other: &Self) -> Option<Self> {

		match (self.orientation, other.orientation) {
			(Orientation::HORIZONTAL, Orientation::HORIZONTAL) => {
				if self.start.y == other.start.y  {
					let start_max = cmp::max(self.start.x, other.start.x);
					let end_max = cmp::min(self.end.x, other.end.x);

					if start_max <= end_max {
						Some(Self {
							start: Point {
								x: start_max,
								y: self.start.y,
							},
							end: Point {
								x: end_max,
								y: self.start.y,
							},
							orientation: if start_max == end_max { Orientation::POINT } else { Orientation::HORIZONTAL },
						})
					} else {
						None
					}
				} else {
					None
				}
			},
			(Orientation::VERTICAL, Orientation::VERTICAL) => {
				if self.start.x == other.start.x {
					let start_max = cmp::max(self.start.y, other.start.y);
					let end_max = cmp::min(self.end.y, other.end.y);

					if start_max <= end_max {
						Some(Self {
							start: Point {
								x: self.start.x,
								y: start_max,
							},
							end: Point {
								x: self.start.x,
								y: end_max,
							},
							orientation: if start_max == end_max { Orientation::POINT } else { Orientation::VERTICAL },
						})
					} else {
						None
					}
				} else {
					None
				}
			},
			(Orientation::DIAGONAL_Y_UP, Orientation::DIAGONAL_Y_UP) => {
				// a bit silly here to keep everything in the unsigned range
				let start_diagonal_origin = match self.start.x.cmp(&self.start.y) {
					Ordering::Greater => Point { x: self.start.x.saturating_sub(self.start.y), y: 0},
					_ => Point { x: 0, y: self.start.x.saturating_sub(self.start.y)},
				};
				let other_diagonal_origin = match other.start.x.cmp(&other.start.y) {
					Ordering::Greater => Point { x: other.start.x.saturating_sub(other.start.y), y: 0},
					_ => Point { x: 0, y: other.start.x.saturating_sub(other.start.y)},
				};

				if start_diagonal_origin != other_diagonal_origin {
					// Not on the same diagonal					
					None
				} else {
					let start_x_max = cmp::max(self.start.x, other.start.x);
					let end_x_max = cmp::min(self.end.x, other.end.x);

					// could find this out mathematically, but lazy
					let start_y_max = cmp::max(self.start.y, other.start.y);
					let end_y_max = cmp::min(self.end.y, other.end.y);


					if start_x_max <= end_x_max {
						Some(Self {
							start: Point {
								x: start_x_max,
								y: start_y_max,
							},
							end: Point {
								x: end_x_max,
								y: end_y_max,
							},
							orientation: if start_x_max == end_x_max { Orientation::POINT } else { Orientation::DIAGONAL_Y_UP },
						})
					} else {
						None
					}

				}

			},
			(Orientation::DIAGONAL_Y_DOWN, Orientation::DIAGONAL_Y_DOWN) => {
				if (self.start.x + self.start.y) != (other.start.x + other.start.y) {
					None
				} else {
					let origin = (self.start.x + self.start.y);
					let start_x_max = cmp::max(self.start.x, other.start.x);
					let end_x_max = cmp::min(self.end.x, other.end.x);

					if start_x_max <= end_x_max {
						Some(Self {
							start: Point {
								x: start_x_max,
								y: origin.saturating_sub(start_x_max),
							},
							end: Point {
								x: end_x_max,
								y: origin.saturating_sub(end_x_max),
							},
							orientation: if start_x_max == end_x_max { Orientation::POINT } else { Orientation::DIAGONAL_Y_DOWN },
						})
					} else {
						None
					}
				}				
			},
			(Orientation::VERTICAL, Orientation::HORIZONTAL) => { other.overlap(self) },
			(Orientation::HORIZONTAL, Orientation::VERTICAL) => {
				let intersection = Point {
					x: other.start.x,
					y: self.start.y,
				};

				if self.point_in_line(intersection) && other.point_in_line(intersection) {
					Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
				} else {
					None
				}
			},
			(Orientation::HORIZONTAL, Orientation::DIAGONAL_Y_DOWN) => { other.overlap(self) },
			(Orientation::DIAGONAL_Y_DOWN, Orientation::HORIZONTAL) => {
				let origin = self.end.x + self.end.y;



				if other.start.y > origin {
					None
				} else {
					let intersection = Point {
						x: origin - other.start.y,
						y: other.start.y,
					};

					if self.point_in_line(intersection) && other.point_in_line(intersection) {
						Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
					} else {
						None
					}
				}
			},
			(Orientation::VERTICAL, Orientation::DIAGONAL_Y_DOWN) => { other.overlap(self) },
			(Orientation::DIAGONAL_Y_DOWN, Orientation::VERTICAL) => {
				let origin = self.end.x + self.end.y;

				if other.start.x > origin {
					None
				} else {
					let intersection = Point {
						x: other.start.x,
						y: origin - other.start.x,
					};

					if self.point_in_line(intersection) && other.point_in_line(intersection) {
						Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
					} else {
						None
					}
				}
			},
			(Orientation::HORIZONTAL, Orientation::DIAGONAL_Y_UP) => { other.overlap(self) },
			(Orientation::DIAGONAL_Y_UP, Orientation::HORIZONTAL) => {

				// fed up with usize
				let y_for_x_zero: isize = (self.start.y as isize) - (self.start.x as isize);

				if ( other.start.y as isize ) < y_for_x_zero {
					None
				} else {
					let intersection = Point {
						x: ((other.start.y as isize) - y_for_x_zero) as usize,
						y: other.start.y,
					};

					if self.point_in_line(intersection) && other.point_in_line(intersection) {
						Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
					} else {
						None
					}
				}
			},
			(Orientation::VERTICAL, Orientation::DIAGONAL_Y_UP) => { other.overlap(self) },
			(Orientation::DIAGONAL_Y_UP, Orientation::VERTICAL) => {

				// fed up with usize
				let x_for_y_zero: isize = (self.start.x as isize) - (self.start.y as isize);

				if ( other.start.x as isize ) < x_for_y_zero {
					None
				} else {
					let intersection = Point {
						x: other.start.x,
						y: ((other.start.x as isize) - x_for_y_zero) as usize,
					};

					if self.point_in_line(intersection) && other.point_in_line(intersection) {
						Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
					} else {
						None
					}
				}
			},
			(Orientation::DIAGONAL_Y_UP, Orientation::DIAGONAL_Y_DOWN) => { other.overlap(self) },
			(Orientation::DIAGONAL_Y_DOWN, Orientation::DIAGONAL_Y_UP) => {
				let origin = self.end.x + self.end.y;

				if other.start.x > other.start.y {
					let x_for_y_zero = other.start.x.saturating_sub(other.start.y);

					if (x_for_y_zero + origin) % 2 != 0 {
						None
					} else if x_for_y_zero > origin {
						None
					} else {
						let y = (origin.saturating_sub(x_for_y_zero)) / 2;
						let intersection = Point {
							x: origin.saturating_sub(y),
							y,
						};

						if self.point_in_line(intersection) && other.point_in_line(intersection) {
							Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
						} else {
							None
						}
					}					
				} else {
					let y_for_x_zero = other.start.y.saturating_sub(other.start.x);

					if (y_for_x_zero + origin) % 2 != 0 {
						None
					} else if y_for_x_zero > origin {
						None
					} else {
						let x = (origin.saturating_sub(y_for_x_zero)) / 2;
						let intersection = Point {
							x,
							y: origin.saturating_sub(x),
						};

						if self.point_in_line(intersection) && other.point_in_line(intersection) {
							Some(Self { start: intersection, end: intersection, orientation: Orientation::POINT})
						} else {
							None
						}
					}
				}
			},			
			(Orientation::POINT, _) if (other.point_in_line(self.start)) => { 
				Some(Self { start: self.start, end: self.end, orientation: Orientation::POINT})
			},
			(_, Orientation::POINT) if (self.point_in_line(other.start)) => { 
				Some(Self { start: other.start, end: other.end, orientation: Orientation::POINT})
			},
			x => {
				println!("{:?}", x);
				None
			}
		}
	}


	fn points(&self) -> Vec<Point> {
		match self.orientation {
			Orientation::HORIZONTAL => {
				(self.start.x..self.end.x+1).map( | x | Point { x, y: self.start.y }).collect()
			},
			Orientation::VERTICAL => {
				(self.start.y..self.end.y+1).map( | y | Point { x: self.start.x, y }).collect()
			},
			Orientation::POINT => {
				vec![self.start]
			},
			Orientation::DIAGONAL_Y_UP => {
				let len = self.end.x.saturating_sub(self.start.x) + 1;
				(0..len).map( | o | Point { x: self.start.x + o, y: self.start.y + o }).collect()
			},
			Orientation::DIAGONAL_Y_DOWN => {
				let len = self.end.x.saturating_sub(self.start.x) + 1;
				(0..len).map( | o | Point { x: self.start.x + o, y: self.start.y - o }).collect()
			},
		}
	}
}



struct LineHolder {
	lines: Vec<Line>,
}


impl LineHolder {
	fn from_file(filename: &str) -> Self {
		let file = File::open(filename).unwrap();	
		let buf_read = io::BufReader::new(file);

		let mut lines: Vec<Line> = Vec::new();
		for line in buf_read.lines() {
			let line = line.unwrap();

			// x,y -> a,b
			// assuming input is formatted correctly ... and code works for it
			let point_split: Vec<&str> = line.split(" -> ").collect();
			let start_str = point_split.first().unwrap();
			let end_str = point_split.last().unwrap();

			let start_split: Vec<&str> = start_str.split(',').collect();
			let point_1 = Point{
				x: start_split.first().unwrap().parse::<usize>().unwrap(),
				y: start_split.last().unwrap().parse::<usize>().unwrap(),
			};

			let end_split: Vec<&str> = end_str.split(',').collect();
			let point_2 = Point{
				x: end_split.first().unwrap().parse::<usize>().unwrap(),
				y: end_split.last().unwrap().parse::<usize>().unwrap(),
			};

			if let Some(l) = Line::from_points(point_1, point_2) {
				lines.push(l);
			}
			
		}

		Self {	lines }
	}


	fn overlap_lines(&self) -> Self {
		let mut overlapped_lines = Self {
			lines: Vec::new(),
		};

		let len = self.lines.len();
		for a_i in 0..len {
			let line_a = self.lines[a_i];
			for b_i in a_i + 1 .. len {
				let line_b = self.lines[b_i];


				
				if let Some(line) = line_a.overlap(&line_b) {
					overlapped_lines.lines.push(line);
					println!("{:?}", line_a);
					println!("{:?}", line_b);
					println!("{}", overlapped_lines.chart_string());
				}
				
			}
		}

		overlapped_lines
	}

	
	fn count_overlap(&self) -> usize {
		let mut point_set: HashSet<Point> = HashSet::new();

		let len = self.lines.len();
		for a_i in 0..len {
			let line_a = self.lines[a_i];
			for b_i in a_i + 1 .. len {
				let line_b = self.lines[b_i];		
				

				if let Some(line) = line_a.overlap(&line_b) {
					for pt in line.points() {
						point_set.insert(pt);
					}
				}
			}
		}

		point_set.len()
	}


	#[allow(dead_code)]
	fn chart_string(&self) -> String {
		let mut points: HashMap<Point, usize> = HashMap::new();
		let mut max_x = 0;
		let mut max_y = 0;

		for line in &self.lines {
			for pt in line.points() {
				max_x = cmp::max(pt.x, max_x);
				max_y = cmp::max(pt.y, max_y);

				if let Some(v) = points.get_mut(&pt) {
					*v += 1;
				} else {
					points.insert(pt, 1);
				}
			}
		}

		let mut output = String::new();
		for y in 0..max_y+1 {
			for x in 0..max_x+1 {
				if let Some(v) = points.get(&Point{ x, y}) {
					output.push_str(&(*v).to_string());
				} else {
					output.push('.');
				}
			}
			output.push('\n');
		}

		output
	}
}


fn main() {
	let line_holder = LineHolder::from_file("../sm_example");
	println!("{}", line_holder.count_overlap() );

}
