use std::fs::File;
use std::io::{self, BufRead};
use std::cmp::{self, Ordering};
use std::collections::{HashSet, HashMap};


#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
struct Point {
	x: usize,
	y: usize
}


#[derive(PartialEq, Copy, Clone, Debug)]
enum Orientation {
	HORIZONTAL,
	VERTICAL,
	POINT,
	DIAGONAL_Y_UP,
	DIAGONAL_Y_DOWN,
}


#[derive(PartialEq, Debug, Clone, Copy)]
struct Line {
	start: Point,
	end: Point,
	orientation: Orientation,
}
impl Line {
	fn from_points(point_1: Point, point_2: Point) -> Option<Self>{
		match (point_1.x.cmp(&point_2.x), point_1.y.cmp(&point_2.y)) {
			(Ordering::Equal, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::POINT,
					start: point_1,
					end: point_1,
				})
			},
			(Ordering::Equal, Ordering::Less) => {
				Some(Self{
					orientation: Orientation::VERTICAL,
					start: point_1,
					end: point_2,
				})
			},
			(Ordering::Equal, Ordering::Greater) => {
				Some(Self{
					orientation: Orientation::VERTICAL,
					start: point_2,
					end: point_1,
				})
			},
			(Ordering::Less, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::HORIZONTAL,
					start: point_1,
					end: point_2,
				})
			},
			(Ordering::Greater, Ordering::Equal) => {
				Some(Self{
					orientation: Orientation::HORIZONTAL,
					start: point_2,
					end: point_1,
				})
			},
			(Ordering::Less, Ordering::Less) => {
				if point_2.x.saturating_sub(point_1.x) == point_2.y.saturating_sub(point_1.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_UP,
						start: point_1,
						end: point_2,
					})		
				} else {
					None
				}
			},
			(Ordering::Greater, Ordering::Greater) => {
				if point_1.x.saturating_sub(point_2.x) == point_1.y.saturating_sub(point_2.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_UP,
						start: point_2,
						end: point_1,
					})		
				} else {
					None
				}
			},
			(Ordering::Less, Ordering::Greater) => {
				if point_2.x.saturating_sub(point_1.x) == point_1.y.saturating_sub(point_2.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_DOWN,
						start: point_1,
						end: point_2,
					})		
				} else {
					None
				}
			},
			(Ordering::Greater, Ordering::Less) => {
				if point_1.x.saturating_sub(point_2.x) == point_2.y.saturating_sub(point_1.y) {
					Some(Self{
						orientation: Orientation::DIAGONAL_Y_DOWN,
						start: point_2,
						end: point_1,
					})		
				} else {
					None
				}
			},
		}
	}

	fn points(&self) -> Vec<Point> {
		match self.orientation {
			Orientation::HORIZONTAL => {
				(self.start.x..self.end.x+1).map( | x | Point { x, y: self.start.y }).collect()
			},
			Orientation::VERTICAL => {
				(self.start.y..self.end.y+1).map( | y | Point { x: self.start.x, y }).collect()
			},
			Orientation::POINT => {
				vec![self.start]
			},
			Orientation::DIAGONAL_Y_UP => {
				let len = self.end.x.saturating_sub(self.start.x) + 1;
				(0..len).map( | o | Point { x: self.start.x + o, y: self.start.y + o }).collect()
			},
			Orientation::DIAGONAL_Y_DOWN => {
				let len = self.end.x.saturating_sub(self.start.x) + 1;
				(0..len).map( | o | Point { x: self.start.x + o, y: self.start.y - o }).collect()
			},
		}
	}
}



struct LineHolder {
	lines: Vec<Line>,
}


impl LineHolder {
	fn from_file(filename: &str) -> Self {
		let file = File::open(filename).unwrap();	
		let buf_read = io::BufReader::new(file);

		let mut lines: Vec<Line> = Vec::new();
		for line in buf_read.lines() {
			let line = line.unwrap();

			// x,y -> a,b
			// assuming input is formatted correctly ... and code works for it
			let point_split: Vec<&str> = line.split(" -> ").collect();
			let start_str = point_split.first().unwrap();
			let end_str = point_split.last().unwrap();

			let start_split: Vec<&str> = start_str.split(',').collect();
			let point_1 = Point{
				x: start_split.first().unwrap().parse::<usize>().unwrap(),
				y: start_split.last().unwrap().parse::<usize>().unwrap(),
			};

			let end_split: Vec<&str> = end_str.split(',').collect();
			let point_2 = Point{
				x: end_split.first().unwrap().parse::<usize>().unwrap(),
				y: end_split.last().unwrap().parse::<usize>().unwrap(),
			};

			if let Some(l) = Line::from_points(point_1, point_2) {
				lines.push(l);
			}
			
		}

		Self {	lines }
	}

	fn count_overlaps(&self) -> usize {
		let mut known_points: HashSet<Point> = HashSet::new();
		let mut counted_points: HashSet<Point> = HashSet::new();
		let mut count: usize = 0;

		for line in &self.lines {
			for pt in line.points() {
				if !known_points.insert(pt) {
					if counted_points.insert(pt) {
						count += 1;
					}
				}
			}
		}

		count
	}

	#[allow(dead_code)]
	fn chart_string(&self) -> String {
		let mut points: HashMap<Point, usize> = HashMap::new();
		let mut max_x = 0;
		let mut max_y = 0;

		for line in &self.lines {
			for pt in line.points() {
				max_x = cmp::max(pt.x, max_x);
				max_y = cmp::max(pt.y, max_y);

				if let Some(v) = points.get_mut(&pt) {
					*v += 1;
				} else {
					points.insert(pt, 1);
				}
			}
		}

		let mut output = String::new();
		for y in 0..max_y+1 {
			for x in 0..max_x+1 {
				if let Some(v) = points.get(&Point{ x, y}) {
					output.push_str(&(*v).to_string());
				} else {
					output.push('.');
				}
			}
			output.push('\n');
		}

		output
	}
}


fn main() {
	let line_holder = LineHolder::from_file("../input");
	// println!("{}\n", line_holder.chart_string() );
	println!("{}", line_holder.count_overlaps() );
}
