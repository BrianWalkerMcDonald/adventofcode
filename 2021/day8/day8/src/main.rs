use std::fs::File;
use std::io::{self, BufRead};
use std::collections::HashMap;

#[derive(Debug, Default)]
struct SignalDigit {
    segments: u8,
    length: usize,
}

impl SignalDigit {
    fn from_string(string: String) -> Self {
        let length = string.len();

        let a_as_byte: u8 = 97;

        let mut segments = 0;
        for b in string.as_bytes() {
            let m = b.saturating_sub(a_as_byte);
            segments |= 1 << m;
        }

        Self {
            segments,
            length,
        }
    }
}

fn process_signal_line(string: String) -> usize {

    let mut gen = string.split_whitespace();

    let mut input: HashMap<usize, Vec<u8>> = Default::default();

    // LEARN: probably a way to advance the iterate by a set amount
    for _ in 0..10 {

        let digit_string = gen.next().unwrap().to_string();
        let digit = SignalDigit::from_string(digit_string);

        let legnth_list = input.entry(digit.length).or_insert(Vec::new());
        legnth_list.push(digit.segments);
    }

    //  aaaa  
    // b    c 
    // b    c 
    //  dddd  
    // e    f 
    // e    f 
    //  gggg  

    let seg_c_f           = *input.get(&2).unwrap().first().unwrap();
    let seg_a_c_f         = *input.get(&3).unwrap().first().unwrap();
    let seg_b_c_d_f       = *input.get(&4).unwrap().first().unwrap();
    let seg_a_b_c_d_e_f_g = *input.get(&7).unwrap().first().unwrap();

    let sixes_list: Vec<u8> = input.get(&6).unwrap().to_vec();

    let seg_b_d = seg_c_f ^ seg_b_c_d_f;
    let seg_c_d_e = sixes_list.into_iter().fold(0, | acc, v | acc | (seg_a_b_c_d_e_f_g ^ v));

    let seg_a = seg_c_f ^ seg_a_c_f;
    let seg_c = seg_c_d_e & seg_c_f;
    let seg_f = seg_c_f ^ seg_c;
    let seg_d = seg_b_d & seg_c_d_e;
    let seg_b = seg_b_d ^ seg_d;
    let seg_e = (seg_c | seg_d) ^ seg_c_d_e;
    let seg_g = seg_a_b_c_d_e_f_g ^ seg_a ^ seg_b ^ seg_c_d_e ^ seg_f;

    // println!("seg_c_f           {:0>7b} {}",seg_c_f, seg_c_f);
    // println!("seg_a_c_f         {:0>7b} {}",seg_a_c_f, seg_a_c_f);
    // println!("seg_b_c_d_f       {:0>7b} {}",seg_b_c_d_f, seg_b_c_d_f);
    // println!("seg_b_d           {:0>7b} {}",seg_b_d, seg_b_d);
    // println!("seg_c_d_e         {:0>7b} {}",seg_c_d_e, seg_c_d_e);
    // println!("seg_a_b_c_d_e_f_g {:0>7b} {}",seg_a_b_c_d_e_f_g, seg_a_b_c_d_e_f_g);


    // println!("{:0>7b} {}",seg_a, seg_a);
    // println!("{:0>7b} {}",seg_c, seg_c);
    // println!("{:0>7b} {}",seg_f, seg_f);
    // println!("{:0>7b} {}",seg_d, seg_d);
    // println!("{:0>7b} {}",seg_b, seg_b);
    // println!("{:0>7b} {}",seg_e, seg_e);
    // println!("{:0>7b} {}",seg_g, seg_g);


    let mut lookup: HashMap<u8, u8> = HashMap::new();

    lookup.insert(seg_a | seg_b | seg_c | seg_e | seg_f | seg_g, 0);
    lookup.insert(seg_c_f, 1);
    lookup.insert(seg_a | seg_c | seg_d | seg_e | seg_g, 2);
    lookup.insert(seg_a | seg_c | seg_d | seg_f | seg_g, 3);
    lookup.insert(seg_b_c_d_f, 4);
    lookup.insert(seg_a | seg_b | seg_d | seg_f | seg_g, 5);
    lookup.insert(seg_a | seg_b | seg_d | seg_e | seg_f | seg_g, 6);
    lookup.insert(seg_a_c_f, 7);
    lookup.insert(seg_a_b_c_d_e_f_g, 8);
    lookup.insert(seg_a | seg_b | seg_c | seg_d | seg_f | seg_g, 9);

    // skip delimiter
    gen.next().unwrap();

    let mut output: usize = 0;

    for i in (0..4).rev() {
        let digit_string = gen.next().unwrap().to_string();
        let digit = SignalDigit::from_string(digit_string);
        let value = *lookup.get(&digit.segments).unwrap();

        output += (value as usize) * 10_usize.pow(i)
    }

    output
}

fn process_signal_file(filename: &str) -> usize {
    let file = File::open(filename).unwrap();   
    let buf_read = io::BufReader::new(file);

    let mut sum = 0;
    for line in buf_read.lines() {
        let line = line.unwrap();

        sum += process_signal_line(line)
    }

    sum
}


fn main() {
    let signal_total = process_signal_file("../input");
    println!("Total: {}", signal_total);
}
