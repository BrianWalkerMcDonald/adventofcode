use day12::CaveNetwork;

pub fn main() {
    let cave_network = CaveNetwork::from_file("../input").expect("Could not load file");

    println!("small caves once path count {}", cave_network.count_paths_small_once());

    println!("small caves twice path count {}", cave_network.count_paths_small_twice());
}