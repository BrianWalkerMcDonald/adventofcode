
use std::fs::File;
use std::io::{BufRead, BufReader};
use std::collections::{HashMap, HashSet};


#[derive(Debug)]
pub enum Error {
	FileLoad(String),
}


#[derive(Hash, Eq, PartialEq, Clone, Copy, Debug)]
struct Cave {
	id: usize,
	is_small: bool,
}


// LEARN: its odd that structs can be constructed by themselves unless they have private vars, how to make empty struct private?
#[derive(Debug)]
pub struct CaveNetwork {
	start: usize,
	end: usize,
	caves: Vec<String>,
	network: HashMap<usize, Vec<Cave>>,
}

impl CaveNetwork {

	pub fn from_file(filename: &str) -> Result<Self, Error> {
		let file = match File::open(filename) {
			Ok(file) => file,
			Err(_) => return Err(Error::FileLoad("Unable to open file".to_string())),
		};

		let buffer = BufReader::new(file);


		let mut answer = Self {
			network: HashMap::new(),
			caves: Vec::new(),
			start: 0,
			end: 0,
		};

		for line in buffer.lines() {

			let line = match line {
				Ok(line) => line,
				Err(_) => return Err(Error::FileLoad("Unable to read line".to_string())),
			};

			let split_line: Vec<&str> = line.split("-").collect();

			if split_line.len() != 2 {
				return Err(Error::FileLoad("Line does not have a single delimiter".to_string()));
			}

			let cave_1 = answer.add_cave(split_line.first().unwrap());
			let cave_2 = answer.add_cave(split_line.last().unwrap());

			answer.add_tunnel(cave_1, cave_2);
		}


		Ok(answer)
	}

	fn add_cave(&mut self, cave_name: &str) -> Cave {
		let name = cave_name.to_string();


		let is_small = name.chars().all(char::is_lowercase);

		let id = match self.caves.iter().position( | x | *x == name ) {
			Some(id) => id,
			None => {
				self.caves.push(name);
				self.caves.len() - 1
			}
		};

		match cave_name {
			"start" => self.start = id,
			"end" => self.end = id,
			_ => (),
		}


		Cave {
			id,
			is_small
		}
	}

	fn add_tunnel(&mut self, cave_1: Cave, cave_2: Cave) {
		self.network.entry(cave_1.id).or_insert(Vec::new()).push(cave_2);
		self.network.entry(cave_2.id).or_insert(Vec::new()).push(cave_1);
	}



	// LEARN: with all these passing &muts, this should have been written as a struct with Iterator trait, that way the struct would hold state
	fn tarverse_path(&self, current_cave: usize, visited_small_caves: &mut HashSet<usize>, total_paths: &mut usize, exempt_small: &mut Option<usize>, route: &mut Vec<usize>, allow_exempt: bool) {

		let neighbors = self.network.get(&current_cave).unwrap();

		for cave in neighbors {
			let cave = *cave;

			if cave.id == self.end {
				// println!("{}", route.iter().map( | &id | (*self.caves.get(id).unwrap()).clone() ).collect::<Vec<String>>().join(",") );

				*total_paths += 1;
				continue;
			}

			if cave.id == self.start {
				continue;
			}

			if cave.is_small {
				let first_visit = visited_small_caves.insert(cave.id);

				if !first_visit {
					if allow_exempt {
						if let Some(_) = *exempt_small {
							continue;
						} else {
							*exempt_small = Some(cave.id)
						}
					} else {
						continue;
					}
				}

			}

			route.push(cave.id);
			self.tarverse_path(cave.id, visited_small_caves, total_paths, exempt_small, route, allow_exempt);
			route.pop();
			if allow_exempt && Some(cave.id) == *exempt_small {
				*exempt_small = None;
			} else {
				visited_small_caves.remove(&cave.id);
			}
		}
	}


	pub fn count_paths_small_once(&self) -> usize {
		let mut total_paths: usize = 0;
		let mut visited_small_caves = HashSet::new();
		let mut route: Vec<usize> = vec![self.start];
		let mut exempt_small = None;
		let allow_exempt = false;

		self.tarverse_path(self.start, &mut visited_small_caves, &mut total_paths, &mut exempt_small, &mut route, allow_exempt);

		total_paths
	}

	pub fn count_paths_small_twice(&self) -> usize {
		let mut total_paths: usize = 0;
		let mut visited_small_caves = HashSet::new();
		let mut route: Vec<usize> = vec![self.start];
		let mut exempt_small = None;
		let allow_exempt = true;

		self.tarverse_path(self.start, &mut visited_small_caves, &mut total_paths, &mut exempt_small, &mut route, allow_exempt);

		total_paths
	}

}


#[cfg(test)]
mod tests {
	use super::*;

    #[test]
    fn test_part1_example_sm() {
        let cave_network = CaveNetwork::from_file("../example_sm").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_once(), 10);
    }


    #[test]
    fn test_part1_example_md() {
        let cave_network = CaveNetwork::from_file("../example_md").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_once(), 19);
    }


    #[test]
    fn test_part1_example_lg() {
        let cave_network = CaveNetwork::from_file("../example_lg").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_once(), 226);
    }

    #[test]
    fn test_part2_example_sm() {
        let cave_network = CaveNetwork::from_file("../example_sm").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_twice(), 36);
    }


    #[test]
    fn test_part2_example_md() {
        let cave_network = CaveNetwork::from_file("../example_md").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_twice(), 103);
    }


    #[test]
    fn test_part2_example_lg() {
        let cave_network = CaveNetwork::from_file("../example_lg").expect("Could not load file");

        assert_eq!(cave_network.count_paths_small_twice(), 3509);
    }
}
