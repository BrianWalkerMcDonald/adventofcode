use std::fs::File;
use std::io::{BufRead, BufReader};


#[derive(Debug, PartialEq, Clone, Copy)]
enum Token {
	ParenClose = 1,
	SquareClose = 2,
	CurlyClose = 3,
	AngleClose = 4,
	ParenOpen,
	SquareOpen,
	CurlyOpen,
	AngleOpen,
	EndOfLine,
}


struct Reader {
	buffer: BufReader<File>,
	current_line: Vec<char>,
}
impl Reader {

	fn from_file(filename: &str) -> Self {
	    let file = File::open(filename).expect("failed to open input file");
	    let buffer = BufReader::new(file);

	    Self {
	    	buffer,
	    	current_line: Vec::new(),
	    }
	}
}

impl Iterator for Reader {
	type Item = char;

	fn next(&mut self) -> Option<Self::Item> {

		if self.current_line.len() == 0 {
			let mut byte_buffer = Vec::new();
			self.buffer.read_until(b'\n', &mut byte_buffer).expect("Unable to read line");

			self.current_line = byte_buffer.iter()
				.rev()
				.map(|b| *b as char)
				.collect();
		}

		self.current_line.pop()
	}
}



struct Lexer {
}

impl Lexer {
	fn parse(c: char) -> Result<Token, char> {
		match c {
			'[' => Ok(Token::SquareOpen),
			']' => Ok(Token::SquareClose),
			'(' => Ok(Token::ParenOpen),
			')' => Ok(Token::ParenClose),
			'{' => Ok(Token::CurlyOpen),
			'}' => Ok(Token::CurlyClose),
			'<' => Ok(Token::AngleOpen),
			'>' => Ok(Token::AngleClose),
			'\n' => Ok(Token::EndOfLine),
			x => Err(x),
		}

	}
}


#[derive(Clone, Copy)]
enum SyntaxError {
	ParenClosed = 3,
	SquareClosed = 57,
	CurlyClosed = 1197,
	AngleClosed = 25137,
}


struct Parser {
	current_error: Option<SyntaxError>,
	next_close_token: Option<Token>,
	context_tracker: Vec<Token>,
	syntax_errors: Vec<SyntaxError>,
	incomplete_scores: Vec<usize>,
}

impl Parser {
	fn new() -> Self {
		Self {
			current_error: None,
			next_close_token: None,
			context_tracker: Vec::new(),
			syntax_errors: Vec::new(),
			incomplete_scores: Vec::new(),
		}
	}

	fn parse(&mut self, token: Token) {

		match (self.current_error, self.next_close_token, token) {

			// Error handling
			(Some(err), _, Token::EndOfLine) => {
				// record line error and clear state
				self.syntax_errors.push(err);
				self.current_error = None;
				self.next_close_token = None;
				self.context_tracker.clear();
			},
			(Some(_), _, _) => {
				// current error, wait until end of line
			},

			// complete line
			(None, None, Token::EndOfLine) => {
				// valid line, clear state
				self.current_error = None;
				self.next_close_token = None;
				self.context_tracker.clear();
			},
			// incomplete line
			(None, Some(end_token), Token::EndOfLine) => {
				self.context_tracker.push(end_token);

				let score: usize = self.context_tracker.iter()
					.rev()
					.fold(0, | acc, token | acc * 5 + ( *token as usize));

				self.incomplete_scores.push(score);

				// clear state
				self.current_error = None;
				self.next_close_token = None;
				self.context_tracker.clear();
			},

			// Close hunk
			(None, Some(end_token), token) if end_token == token => {
				self.next_close_token = self.context_tracker.pop();
			},

			// Invalid close
			(None, _, token @ (Token::SquareClose | Token::ParenClose | Token::CurlyClose | Token::AngleClose) ) => {
				self.current_error = match token {
					Token::SquareClose => Some(SyntaxError::SquareClosed),
					Token::ParenClose => Some(SyntaxError::ParenClosed),
					Token::CurlyClose => Some(SyntaxError::CurlyClosed),
					Token::AngleClose => Some(SyntaxError::AngleClosed),
					_ => panic!("Shouldn't every get here"),
				};
			},

			// Opening
			(None, _, token @ (Token::SquareOpen | Token::ParenOpen | Token::CurlyOpen | Token::AngleOpen) ) => {

				// record outer context
				if let Some(end_token) = self.next_close_token {
					self.context_tracker.push(end_token)
				}

				self.next_close_token = match token {
					Token::SquareOpen => Some(Token::SquareClose),
					Token::ParenOpen => Some(Token::ParenClose),
					Token::CurlyOpen => Some(Token::CurlyClose),
					Token::AngleOpen => Some(Token::AngleClose),
					_ => panic!("Shouldn't every get here"),
				};
			},
		}
	}


	fn syntax_error_score(&self) -> usize {
		self.syntax_errors.iter().map( | e | *e as usize ).sum()
	}

	fn incomplete_error_score(&self) -> usize {

		// LEARN: probably a way to use binary heap so inserts are add in a sorted manner
		let mut sorted = self.incomplete_scores.clone();
		sorted.sort();
		let half_index = sorted.len() / 2;

		sorted[half_index]
	}
}



fn main() {
    let reader = Reader::from_file("../input");

    let mut parser = Parser::new();

    for c in reader {
    	let token = Lexer::parse(c).unwrap();
    	
    	parser.parse(token);
    }

    println!("Error score {}", parser.syntax_error_score());
    println!("Incomplete score {}", parser.incomplete_error_score());

}
